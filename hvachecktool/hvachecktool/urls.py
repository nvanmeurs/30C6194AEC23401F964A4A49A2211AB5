from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
                       url(r'^auth/', include('api_client.urls')),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^api/', include('api.urls')),
                       url(r'^$', include('frontend.urls')),
                       )

admin_title = 'Checktool administration'
admin.site.site_header = admin_title
admin.site.site_title = admin_title
admin.site.index_title = ''