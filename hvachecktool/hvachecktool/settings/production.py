from hvachecktool.settings.default import *

SECRET_KEY = os.environ['HVACHECKTOOL_SECRET_KEY']

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ['*']

STATIC_ROOT = '/webapps/hvachecktool/static'

# Email these people when an error occurs
ADMINS = (('Nicky', 'nvmeurs@outlook.com'),)
MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'hvachecktool',
        'USER': 'hvachecktool',
        'PASSWORD': 'tX6RQlA0GYzGnvmrOhow7W7fDERMuFku5EEQxQVTzSwHwtypoDR0lDUys9S8mTn',
        'HOST': '/var/run/mysqld/mysqld.sock',
    }
}

SITE_ID = 1