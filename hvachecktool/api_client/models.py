from allauth.socialaccount.models import SocialAccount
from django.db import models
from django.contrib.auth.models import User
from django.utils.encoding import python_2_unicode_compatible

@python_2_unicode_compatible
class Controlrun(models.Model):
    student = models.ForeignKey('Student', related_name='controlruns')
    timestamp = models.CharField(max_length=20)

    def __str__(self):
        return self.timestamp


@python_2_unicode_compatible
class Course(models.Model):
    description = models.TextField(max_length=255)

    def __str__(self):
        return self.description


@python_2_unicode_compatible
class Group(models.Model):
    schoolyear = models.ForeignKey('Schoolyear', related_name='groups')
    exact_user = models.ForeignKey('ExactUser', related_name='groups')
    course = models.ForeignKey('Course', related_name='groups', blank=True, null=True)
    description = models.CharField(max_length=60)

    def __str__(self):
        return self.description

    class Meta:
        verbose_name = 'class'
        verbose_name_plural = 'classes'


@python_2_unicode_compatible
class Result(models.Model):
    controlrun = models.ForeignKey('Controlrun', related_name='results')
    api = models.CharField(max_length=45, blank=True)
    code = models.CharField(max_length=45, blank=True)
    field = models.CharField(max_length=45, blank=True)
    result = models.CharField(max_length=45, blank=True)
    message = models.TextField(blank=True)

    def __str__(self):
        return self.message


@python_2_unicode_compatible
class School(models.Model):
    schoolname = models.CharField(max_length=45, blank=True)

    def __str__(self):
        return self.schoolname


@python_2_unicode_compatible
class Schoolyear(models.Model):
    school = models.ForeignKey('School', related_name='schoolyears')
    schoolyear = models.CharField(max_length=20)
    current_fy = models.IntegerField()
    new_fy = models.IntegerField()

    def __str__(self):
        return self.schoolyear


@python_2_unicode_compatible
class Student(models.Model):
    group = models.ForeignKey('Group', related_name='students', blank=True, null=True)
    division = models.CharField(max_length=8)
    account = models.ForeignKey(SocialAccount, related_name='student')

    def __str__(self):
        if self.group is not None:
            return '%s %s' % (self.division, self.group.description)
        else:
            return self.division


class ExactUser(models.Model):
    user = models.OneToOneField(User, related_name='exactuser')
    school = models.ForeignKey('School', related_name='exactusers')
    preferred_group = models.ForeignKey('Group', related_name='preferred_by', blank=True, null=True)

    class Meta:
        verbose_name = 'teacher'
        verbose_name_plural = 'teachers'