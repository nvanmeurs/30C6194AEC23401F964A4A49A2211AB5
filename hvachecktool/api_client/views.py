from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.contrib.auth import logout as auth_logout
from django.shortcuts import redirect, render


@login_required
def logout(request):
    auth_logout(request)
    return render(request, 'frontend/logout.html')

def activate(request):
    if request.user.is_authenticated():
        socialaccount = request.user.socialaccount_set.first()
        data = socialaccount.extra_data
        auth_logout(request)
        msg = 'Student met gebruikersnaam %s en administratie %s is geactiveerd.<br /><br />' \
              % (data['UserName'], data['CurrentDivision'])
        msg += '<strong>Volg a.u.b. de volgende stappen om nog een account te activeren:</strong> ' \
               '<ol><li><a target="_blank" href="https://start.exactonline.nl/docs/Clearsession.aspx">' \
               'Klik hier om uit te loggen op ExactOnline</a></li>'
        msg += '<li>Login op ExactOnline met het account dat u wilt activeren.</li>'
        msg += '<li>Vernieuw deze pagina (druk op de F5 toets).</li></ol>'
        return HttpResponse(msg)
    else:
        return redirect('/auth/login/?next=%s' % request.path)