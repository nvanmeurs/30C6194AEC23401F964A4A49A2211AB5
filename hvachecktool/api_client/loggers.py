import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler('api_calls.log')
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s:%(levelname)s] %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


def log_successful_call(response, **kwargs):
    logger.debug('URL: %r' % response.url)
    logger.debug('Status code: %r' % response.status_code)
    logger.debug('Headers: %r' % response.headers)
    logger.debug('Content: %r' % response.content)
    logger.debug('____________________________')


def log_failed_call(request, errno, errmsg, **kwargs):
    logger.error('Request: %r failed.' % request)
    logger.error('Error: %r (%r)' % (errno, errmsg))
    logger.error('____________________________')