# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api_client', '0009_student_account'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='exactuser',
            options={'verbose_name': 'teacher', 'verbose_name_plural': 'teachers'},
        ),
        migrations.AlterModelOptions(
            name='group',
            options={'verbose_name': 'class', 'verbose_name_plural': 'classes'},
        ),
    ]
