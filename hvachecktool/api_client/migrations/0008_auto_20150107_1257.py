# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api_client', '0007_auto_20141223_1108'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exactuser',
            name='preferred_group',
            field=models.ForeignKey(related_name='preferred_by', blank=True, to='api_client.Group', null=True),
            preserve_default=True,
        ),
    ]
