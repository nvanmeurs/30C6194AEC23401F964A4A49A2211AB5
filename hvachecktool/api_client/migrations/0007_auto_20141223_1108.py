# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('api_client', '0006_auto_20141223_1033'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='exactuser',
            name='group',
        ),
        migrations.RemoveField(
            model_name='student',
            name='name',
        ),
        migrations.AlterField(
            model_name='student',
            name='group',
            field=models.ForeignKey(related_name='students', blank=True, to='api_client.Group', null=True),
            preserve_default=True,
        ),
    ]
