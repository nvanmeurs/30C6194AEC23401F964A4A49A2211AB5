# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('socialaccount', '__first__'),
        ('api_client', '0008_auto_20150107_1257'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='account',
            field=models.ForeignKey(related_name='student', default=1, to='socialaccount.SocialAccount'),
            preserve_default=False,
        ),
    ]
