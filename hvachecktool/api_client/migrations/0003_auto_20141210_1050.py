# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('api_client', '0002_answer_additional_text'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='answer',
            options={'ordering': ['assignment_sub2']},
        ),
        migrations.RemoveField(
            model_name='answer',
            name='desc',
        ),
        migrations.RemoveField(
            model_name='answer',
            name='sequence',
        ),
        migrations.AddField(
            model_name='answer',
            name='assignment_sub2',
            field=models.CharField(default=b'', max_length=60),
            preserve_default=True,
        ),
    ]
