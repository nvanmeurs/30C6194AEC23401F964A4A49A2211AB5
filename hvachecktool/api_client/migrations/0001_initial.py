# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sequence', models.IntegerField(unique=True)),
                ('parent_desc', models.CharField(max_length=255, blank=True)),
                ('topic', models.CharField(max_length=60)),
                ('assignment', models.CharField(max_length=60)),
                ('assignment_sub1', models.CharField(max_length=60)),
                ('api', models.CharField(max_length=60)),
                ('field', models.CharField(max_length=60)),
                ('desc', models.CharField(max_length=255)),
                ('value', models.CharField(max_length=255)),
                ('checktype', models.IntegerField()),
            ],
            options={
                'ordering': ['sequence'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Controlrun',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.CharField(max_length=20)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ExactUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=60)),
                ('exact_user', models.ForeignKey(to='api_client.ExactUser')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Result',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('api', models.CharField(max_length=45, blank=True)),
                ('code', models.CharField(max_length=45, blank=True)),
                ('field', models.CharField(max_length=45, blank=True)),
                ('result', models.CharField(max_length=45, blank=True)),
                ('message', models.TextField(blank=True)),
                ('controlrun', models.ForeignKey(to='api_client.Controlrun')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='School',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('schoolname', models.CharField(max_length=45, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Schoolyear',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('schoolyear', models.CharField(max_length=20)),
                ('current_fy', models.IntegerField()),
                ('new_fy', models.IntegerField()),
                ('school', models.ForeignKey(to='api_client.School')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('division', models.CharField(max_length=8)),
                ('group', models.ForeignKey(to='api_client.Group')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='group',
            name='schoolyear',
            field=models.ForeignKey(to='api_client.Schoolyear'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='exactuser',
            name='preferred_group',
            field=models.ForeignKey(blank=True, to='api_client.Group', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='exactuser',
            name='school',
            field=models.ForeignKey(to='api_client.School'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='exactuser',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='controlrun',
            name='student',
            field=models.ForeignKey(to='api_client.Student'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='answer',
            name='group',
            field=models.ForeignKey(to='api_client.Group'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='answer',
            name='parent',
            field=models.ForeignKey(blank=True, to='api_client.Answer', null=True),
            preserve_default=True,
        ),
    ]
