# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):
    dependencies = [
        ('api_client', '0003_auto_20141210_1050'),
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.TextField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='answer',
            name='group',
        ),
        migrations.AddField(
            model_name='answer',
            name='course',
            field=models.ForeignKey(related_name='answers', blank=True, to='api_client.Course', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='group',
            name='course',
            field=models.ForeignKey(related_name='groups', blank=True, to='api_client.Course', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='answer',
            name='parent',
            field=models.ForeignKey(related_name='children', blank=True, to='api_client.Answer', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='controlrun',
            name='student',
            field=models.ForeignKey(related_name='controlruns', to='api_client.Student'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='exactuser',
            name='school',
            field=models.ForeignKey(related_name='exactusers', to='api_client.School'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='exactuser',
            name='user',
            field=models.OneToOneField(related_name='exactuser', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='group',
            name='exact_user',
            field=models.ForeignKey(related_name='groups', to='api_client.ExactUser'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='group',
            name='schoolyear',
            field=models.ForeignKey(related_name='groups', to='api_client.Schoolyear'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='result',
            name='controlrun',
            field=models.ForeignKey(related_name='results', to='api_client.Controlrun'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='schoolyear',
            name='school',
            field=models.ForeignKey(related_name='schoolyears', to='api_client.School'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='student',
            name='group',
            field=models.ForeignKey(related_name='students', to='api_client.Group'),
            preserve_default=True,
        ),
    ]
