# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('api_client', '0004_auto_20141211_1339'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='answer',
            name='course',
        ),
        migrations.RemoveField(
            model_name='answer',
            name='parent',
        ),
        migrations.DeleteModel(
            name='Answer',
        ),
        migrations.AddField(
            model_name='student',
            name='name',
            field=models.CharField(default=b'', max_length=60, blank=True),
            preserve_default=True,
        ),
    ]
