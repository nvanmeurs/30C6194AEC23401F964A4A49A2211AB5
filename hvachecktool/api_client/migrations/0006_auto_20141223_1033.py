# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('api_client', '0005_auto_20141222_1040'),
    ]

    operations = [
        migrations.AddField(
            model_name='exactuser',
            name='group',
            field=models.ForeignKey(related_name='students', blank=True, to='api_client.Group', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='student',
            name='group',
            field=models.ForeignKey(related_name='students_old', to='api_client.Group'),
            preserve_default=True,
        ),
    ]
