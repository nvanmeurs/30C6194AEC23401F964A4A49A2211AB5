from django.apps import AppConfig

class ApiClientConfig(AppConfig):
    name = 'api_client'

    def ready(self):
        import api_client.signals