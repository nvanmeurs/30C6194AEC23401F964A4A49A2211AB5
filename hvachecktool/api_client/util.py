from addict import Dict
import json
import xmltodict
from datetime import datetime


def decode_json_content(response):
    return json.loads(response.content)


def decode_xml_content(response):
    return xmltodict.parse(response.content)


def filter_api_value(value):
    if isinstance(value, bool):
        value = '%s' % value
    if isinstance(value, basestring):
        if len(value) == 21 and value[1:5] == 'Date':
            value = datetime.utcfromtimestamp(int(value[6:16])).isoformat()

    return unicode(value)


def build_query_string(params):
    if not params:
        return ''

    query_string = '?%s' % '&'.join(['%s=%s' % (param, params[param]) for param in params])

    return query_string.encode('latin_1')


def build_headers(content_type=None, access_token=None, extra_headers=None):
    headers = Dict()

    if content_type == 'json':
        headers['Accept'] = 'application/json'
    elif content_type == 'xml':
        headers['Accept'] = 'application/xml'

    if access_token:
        bearer = 'Bearer %s' % access_token
        headers['Authorization'] = bearer.encode('latin_1')

    if extra_headers:
        headers.update(extra_headers)

    return headers


def get_student_names(access_token, students):
    from api_client.async import AsyncClient
    from api_client import endpoints
    client = AsyncClient()
    for student in students:
        client.add_call(endpoints.Users(
            access_token=access_token,
            student_division=student.division,
            guid=student.account.uid))

    client.start()
    return {client.results.users[student]['UserID']: client.results.users[student]['FullName'] for student in client.results.users }