from django.conf.urls import patterns, url, include

from api_client import views
from api_client.provider import ExactProvider, oauth2_login, oauth2_callback

urlpatterns = patterns('',
                       url(r'^accounts/', include('allauth.urls')),
                       url(r'^logout/$', views.logout, name="logout"),
                       url(r'^login/$', oauth2_login, name=ExactProvider.id + "_login"),
                       url(r'^login/callback/$', oauth2_callback, name=ExactProvider.id + "_callback"),
                       url(r'^activate/$', views.activate, name='activate'),
                       )
