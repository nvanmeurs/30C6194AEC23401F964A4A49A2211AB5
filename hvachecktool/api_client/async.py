from human_curl.async import AsyncClient as CurlAsyncClient
from addict import Dict


class AsyncClient(CurlAsyncClient):
    def __init__(self, *args, **kwargs):
        super(AsyncClient, self).__init__(*args, **kwargs)
        self.results = Dict()

    def add_call(self, endpoint):
        self.method(
            method=endpoint.method,
            url=endpoint.url,
            headers=endpoint.headers,
            data=endpoint.data,
            success_callback=endpoint.on_success,
            fail_callback=endpoint.on_fail
        )