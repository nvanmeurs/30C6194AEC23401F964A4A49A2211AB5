from allauth.socialaccount.helpers import render_authentication_error, complete_social_login
from allauth.socialaccount.models import SocialLogin
from allauth.socialaccount.providers.oauth2.provider import OAuth2Provider
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect
from oauthlib.oauth2 import OAuth2Error
import requests
from allauth.socialaccount.providers.base import ProviderAccount, AuthAction
from allauth.socialaccount.providers.oauth2.views import OAuth2Adapter, OAuth2LoginView, OAuth2CallbackView, OAuth2View
from allauth.socialaccount import providers
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter

import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler('auth.log')
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s:%(levelname)s] %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

class ExactAccount(ProviderAccount):
    pass


class ExactProvider(OAuth2Provider):
    id = 'exact'
    name = 'Exact'
    package = 'api_client.provider'
    account_class = ExactAccount

    def get_default_scope(self):
        scope = ['profile']
        return scope

    def extract_uid(self, data):
        return str(data['d']['results'][0]['UserID'])

    def extract_extra_data(self, data):
        return data['d']['results'][0]

    def extract_common_fields(self, data):
        return {'username': str(data['d']['results'][0]['UserID'])}


class ExactOAuth2Adapter(DefaultSocialAccountAdapter, OAuth2Adapter):
    provider_id = ExactProvider.id
    access_token_url = 'https://start.exactonline.nl/api/oauth2/token'
    authorize_url = 'https://start.exactonline.nl/api/oauth2/auth'
    profile_url = 'https://start.exactonline.nl/api/v1/current/Me'
    expires_in_key = 'expires_in'

    def complete_login(self, request, app, token, **kwargs):
        headers = {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token.token,
        }
        response = requests.get(self.profile_url, headers=headers)
        extra_data = response.json()
        return self.get_provider().sociallogin_from_response(
            request,
            extra_data)


oauth2_login = OAuth2LoginView.adapter_view(ExactOAuth2Adapter)
oauth2_callback = OAuth2CallbackView.adapter_view(ExactOAuth2Adapter)
providers.registry.register(ExactProvider)