var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var path = require('path');
var del = require('del');

var frontendDir = './frontend/static/frontend';
var getPath = function(dir) {
    return path.join(frontendDir, dir);
};

var cssPaths = [
    getPath('themes/basic/css/styles.css'),
    getPath('themes/basic/demo/variations/default.css'),
    getPath('themes/basic/demo/variations/header-red.css'),
    getPath('themes/basic/plugins/codeprettifier/prettify.css'),
    getPath('themes/basic/plugins/form-toggle/toggles.css'),
    getPath('bower_components/html5-boilerplate/css/normalize.css'),
    getPath('bower_components/html5-boilerplate/css/main.css'),
    getPath('css/app.css'),
    getPath('themes/basic/fonts/font-awesome/css/font-awesome.css')
];
var jsPaths = [
    getPath('app/**/*.js'),
    getPath('common/**/*.js'),
    getPath('themes/basic/js/enquire.js'),
    getPath('themes/basic/js/jquery.cookie.js'),
    getPath('themes/basic/plugins/codeprettifier/prettify.js'),
    getPath('themes/basic/js/placeholdr.js'),
    getPath('themes/basic/js/application.js'),
    getPath('themes/basic/demo/demo.js'),
    getPath('themes/basic/js/dojo/dojo.js'),
    getPath('themes/basic/js/dojox/main.js'),
    getPath('themes/basic/js/dojox/json/query.js')
];

gulp.task('optimize', ['styles', 'scripts']);

gulp.task('styles', function() {
   gulp.src(cssPaths)
       .pipe($.watch(cssPaths))
       .pipe($.rename({suffix: '.min'}))
       .pipe($.minifyCss())
       .pipe(gulp.dest(getPath('minified')))
});

gulp.task('scripts', function() {
   gulp.src(jsPaths)
       .pipe($.watch(jsPaths))
       .pipe($.ngAnnotate())
       .pipe($.rename({suffix: '.min'}))
       .pipe($.uglify())
       .pipe(gulp.dest(getPath('minified')));
});

gulp.task('clean:pyc', function(cb) {
    del(['**/*.pyc'], cb);
});

gulp.task('clean:js', function(cb) {
    del([getPath('minified/**/*.js')], cb);
});

gulp.task('clean:css', function(cb) {
    del([getPath('minified/**/*.css')], cb);
});

gulp.task('clean', ['clean:pyc', 'clean:css', 'clean:js']);

gulp.task('default', ['optimize']);
