from django.contrib import admin

from api_client.models import Group, ExactUser, School, Schoolyear, Student, Course
from api.models import Assignment, Answer
from allauth.socialaccount.models import *
from allauth.account.models import *
from django.contrib.auth.models import User
from django.contrib.auth.models import Group as AuthGroup
from django.contrib.sites.models import *


class AnswerInline(admin.TabularInline):
    model = Answer
    extra = 0


@admin.register(Assignment)
class AssignmentAdmin(admin.ModelAdmin):
    list_display = ['subassignment', 'description', 'check_type', 'course']
    search_fields = ['topic_number', 'assignment_number', 'subassignment_number', 'description', 'course']
    inlines = [AnswerInline]


@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = ['answer', 'api_name', 'api_field', 'correct_answer']
    search_fields = ['assignment', 'api_name', 'api_field']
    list_filter = ['assignment', 'api_name', 'api_field']


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ['division', 'account', 'group']
    list_select_related = ['account', 'group']
    list_filter = ['group']
    search_fields = ['division']


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    list_display = ['description']
    search_fields = ['description']


@admin.register(ExactUser)
class ExactUserAdmin(admin.ModelAdmin):
    list_display = ['user', 'school', 'preferred_group']
    list_select_related = ['user', 'school', 'preferred_group']
    list_filter = ['school']


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ['description', 'course', 'schoolyear', 'exact_user']
    list_select_related = ['course', 'schoolyear', 'exact_user']
    list_filter = ['course', 'schoolyear']

@admin.register(School)
class SchoolAdmin(admin.ModelAdmin):
    list_display = ['schoolname']
    search_fields = ['schoolname']

@admin.register(Schoolyear)
class SchoolyearAdmin(admin.ModelAdmin):
    list_display = ['schoolyear', 'school', 'current_fy', 'new_fy']
    list_select_related = ['school']

admin.site.unregister(SocialAccount)
admin.site.unregister(SocialApp)
admin.site.unregister(SocialToken)
admin.site.unregister(EmailAddress)
admin.site.unregister(EmailConfirmation)
admin.site.unregister(User)
admin.site.unregister(AuthGroup)
admin.site.unregister(Site)