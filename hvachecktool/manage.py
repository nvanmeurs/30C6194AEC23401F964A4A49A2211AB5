#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":

    # Checktool is running in production when the HVACHECKTOOL_SECRET_KEY environment is available.
    if 'HVACHECKTOOL_SECRET_KEY' in os.environ:
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hvachecktool.settings.production")
    else:
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hvachecktool.settings.development")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
