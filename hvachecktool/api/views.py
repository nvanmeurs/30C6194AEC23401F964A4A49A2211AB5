import json

from addict import Dict
from allauth.socialaccount.models import SocialAccount
from rest_framework.views import APIView
from django.http import HttpResponse
from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.response import Response
import datetime
from api_client.auth import Auth
from api_client.models import Group, Student
from api.serializers import GroupModelSerializer, StudentModelSerializer, CurrentMeSerializer
from api_client.async import AsyncClient
from api.permissions import IsAuthenticatedAndHasValidAccessToken
from api_client.util import get_student_names
from api_client import endpoints
from api.results import get_check_results
from api.models import FlaggedUser


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all().only('description')
    serializer_class = GroupModelSerializer
    permission_classes = [IsAuthenticatedAndHasValidAccessToken]


class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.all()\
        .select_related('account__uid', 'group__description', 'group__schoolyear')\
        .only('id', 'division', 'group__description',
              'account__uid', 'group__schoolyear__current_fy', 'group__schoolyear__new_fy')
    serializer_class = StudentModelSerializer
    permission_classes = [IsAuthenticatedAndHasValidAccessToken]
    _names = None
    ordering = ('name',)

    def get_serializer_context(self):
        return {
            'request': self.request,
            'format': self.format_kwarg,
            'view': self,
            'names': self._names
        }

    def get_queryset(self):
        group = self.request.GET.get('group_id', None)

        if group is not None:
            if group == 'null':
                return self.queryset.filter(group=None)

            return self.queryset.filter(group=group)

        return self.queryset

    def _get_names(self, access_token, students):
        self._names = get_student_names(access_token, students)

    def list(self, request, *args, **kwargs):
        auth = Auth(request.user)
        access_token = auth.tokens.access_token

        self.object_list = self.filter_queryset(self.get_queryset())

        group_id = kwargs.get('pk', None)

        if group_id is not None:
            if group_id == 'null':
                self.object_list = self.object_list.filter(group_id=None)
            else:
                self.object_list = self.object_list.filter(group_id=group_id)

        self._get_names(access_token, self.object_list)

        serializer = self.get_serializer(self.object_list, many=True)

        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        auth = Auth(request.user)
        access_token = auth.tokens.access_token

        self._get_names(access_token, [self.get_object()])

        serializer = self.get_serializer(self.get_object())

        return Response(serializer.data)

    @detail_route(methods=['get'])
    def results(self, request, pk=None, **kwargs):
        self.object = self.get_object()

        result_requester = request.user
        result_requester_accounts = SocialAccount.objects.filter(user_id=result_requester.id)

        student = Student.objects.get(pk=pk)
        student_account = student.account
        second_of_february = datetime.date(2015, 2, 2)

        for result_requester_account in result_requester_accounts:
            student_is_not_the_requester = student_account.user_id != result_requester_account.user_id
            student_is_not_active = student_account.last_login.date() < second_of_february

            if student_is_not_the_requester and student_is_not_active:

                flagged_user = FlaggedUser(
                    checked_by=result_requester_account,
                    student_checked=student_account)
                flagged_user.save()

        auth = Auth(result_requester)
        access_token = auth.tokens.access_token
        partner_key = auth.tokens.partner_key

        try:
            current_financial_year = self.object.group.schoolyear.current_fy
        except:
            current_financial_year = 2015

        try:
            next_financial_year = self.object.group.schoolyear.new_fy
        except:
            next_financial_year = 2016

        check_results = get_check_results(
            access_token=access_token,
            partner_key=partner_key,
            current_financial_year=current_financial_year,
            next_financial_year=next_financial_year,
            division=self.object.division
        )

        return Response([check_results])
        # return HttpResponse(json.dumps([check_results]), content_type="application/json")

    @detail_route(methods=['get'])
    def group_results(self, request, pk=None, **kwargs):
        self.object = self.get_object()
        auth = Auth(request.user)
        access_token = auth.tokens.access_token
        partner_key = auth.tokens.partner_key

        try:
            current_financial_year = self.object.group.schoolyear.current_fy
        except:
            current_financial_year = 2015

        try:
            next_financial_year = self.object.group.schoolyear.new_fy
        except:
            next_financial_year = 2016

        self._get_names(access_token=access_token, students=[self.object])

        check_results = Dict()

        student_results = get_check_results(
            access_token=access_token,
            partner_key=partner_key,
            current_financial_year=current_financial_year,
            next_financial_year=next_financial_year,
            division=self.object.division
        )

        uid = self.object.account.uid
        check_results[self.object.id].name = self._names[uid] if uid in self._names else None

        for topic in student_results:
            check_results[self.object.id][topic].topic = topic

            for assignment in student_results[topic]:
                errorcount = 0
                successcount = 0

                for subassignment in student_results[topic][assignment]:
                    for subsubassignment in student_results[topic][assignment][subassignment]:
                        if subsubassignment == 'desc':
                            continue

                        if student_results[topic][assignment][subassignment][subsubassignment]['result'] == 0:
                            errorcount += 1
                        else:
                            successcount += 1

                result = 1 if errorcount == 0 else 0
                totalcount = successcount + errorcount
                errorcount = errorcount if errorcount != 0 else 'N/A'

                check_results[self.object.id][topic][assignment].assignment = assignment
                check_results[self.object.id][topic][assignment].errorcount = errorcount
                check_results[self.object.id][topic][assignment].successcount = successcount
                check_results[self.object.id][topic][assignment].totalcount = totalcount
                check_results[self.object.id][topic][assignment].result = result

        return Response([check_results])
        # return HttpResponse(json.dumps([check_results]), content_type="application/json")


class CurrentMe(APIView):
    def get(self, request):
        auth = Auth(request.user)
        access_token = auth.tokens.access_token
        client = AsyncClient()

        client.add_call(
            endpoints.CurrentMe(access_token=access_token)
        )

        client.start()

        results = client.results.current_me[0]

        group_name = request.user.groups.first().name

        if group_name == 'teacher':
            school_name = request.user.exactuser.school.schoolname
        elif group_name == 'student':
            school_name = request.user.account.student.group.schoolyear.school.schoolname
        else:
            school_name = None

        current_me = Dict()
        current_me.username = results['UserName']
        current_me.exact_uid = results['UserID']
        current_me.full_name = results['FullName']
        current_me.current_division = results['CurrentDivision']
        current_me.school_name = school_name
        current_me.role = group_name

        serializer = CurrentMeSerializer(current_me)

        return Response([serializer.data])


class DebugApi(APIView):
    def get(self, request):
        auth = Auth(request.user)

        debug_api = Dict()

        debug_api._meta.auth.old_access_token = auth.tokens.access_token
        auth.tokens.refresh()
        debug_api._meta.auth.new_access_token = auth.tokens.access_token

        access_token = auth.tokens.access_token
        partner_key = auth.tokens.partner_key

        debug_api._meta.auth.partner_key = partner_key

        division = request.GET.get('division', 692197)
        current_financial_year = request.GET.get('current_financial_year', 2014)
        next_financial_year = request.GET.get('next_financial_year', 2015)
        student_division = request.GET.get('student_division', 692197)

        debug_api._meta.params.division = division
        debug_api._meta.params.current_financial_year = current_financial_year
        debug_api._meta.params.next_financial_year = next_financial_year
        debug_api._meta.params.student_division = student_division
        debug_api._meta.params._info = \
            'Change the params using a GET parameter, ex: /api/debug/?division=900&next_financial_year=2036'

        client = AsyncClient()

        for endpoint in [
            endpoints.CurrentMe(access_token=access_token),
            endpoints.GLAccounts(access_token=access_token, division=division),
            endpoints.GLAccounts(access_token=access_token, division=division, page=2),
            endpoints.Journals(access_token=access_token, division=division),
            endpoints.VATs(access_token=access_token, division=division, partner_key=partner_key),
            endpoints.PaymentConditions(access_token=access_token, division=division),
            endpoints.Accounts(access_token=access_token, division=division),
            endpoints.Contacts(access_token=access_token, division=division),
            endpoints.ItemGroups(access_token=access_token, division=division),
            endpoints.Items(access_token=access_token, division=division),
            endpoints.BalanceByYear(access_token=access_token, division=division,
                                    current_financial_year=current_financial_year),
            endpoints.FinancialPeriods(access_token=access_token, division=division,
                                       next_financial_year=next_financial_year),
            endpoints.PayablesList(access_token=access_token, division=division),
            endpoints.ReceivablesList(access_token=access_token, division=division),
            endpoints.Settings(access_token=access_token, division=division, partner_key=partner_key),
            endpoints.TransactionLines60(access_token=access_token, division=division),
            endpoints.TransactionLines70(access_token=access_token, division=division),
            endpoints.TransactionLines90(access_token=access_token, division=division),
            endpoints.TransactionLines20(access_token=access_token, division=division),
        ]:
            client.add_call(endpoint)

        client.start()

        results = client.results

        debug_api.api_results = results

        return HttpResponse(json.dumps(debug_api, sort_keys=True), content_type='application/json')