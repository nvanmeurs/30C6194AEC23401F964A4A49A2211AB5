ANSWER_DESCRIPTIONS = {
    'Code': 'Code',
    'Type': 'Type',
    'Description': 'Omschrijving',
    'GLAccountCode': 'Grootbroekrekening',
    'GLToPayCode': 'Af te dragen BTW',
    'GLToClaimCode': 'Te ontvangen BTW',
    'Percentage': 'Percentage',
    'VATBoxLink': 'BTW koppeling',
    'PaymentDays': 'Vervaldatum',
    'Name': 'Naam',
    'AddressLine1': 'Adres',
    'City': 'Stad',
    'Status': 'Status',
    'IsSupplier': 'Leverancier',
    'IsSales': 'Klant',
    'VATNumber': 'BTW nummer',
    'PaymentConditionPurchase': 'Betalingsconditie',
    'GLAccountPurchase': 'Grootboekrekening',
    'PurchaseVATCode': 'BTW code',
    'AccountName': 'Relatie',
    'FullName': 'Naam',
    'GLRevenueCode': 'Grootboekrekening',
    'GLRevenueDescription': 'Grootboekrekening omschrijving',
    'ItemGroupCode': 'Artikelgroep',
    'SalesPrice': 'Verkoopprijs',
    'SalesVatCode': 'BTW code',
    'UnitDescription': 'Eenheid',
    'IsFractionAllowedItem': 'Deelbaar toegestaan',
    'StartDate': 'Begindatum',
    'EndDate': 'Einddatum',
    'FinPeriod': 'Financiele periode',
    'GLDiscountSales': 'Verkoopkortingen',
    'GLPaymentDifferencePurchase': 'Betalingsverschillen Inkoop',
    'GLDiscountPurchase': 'Inkoopkortingen',
    'PaymentPurchase': 'Betalingsconditie Inkoop',
    'PaymentSales': 'Betalingsconditie Verkoop',
    'AllowReopenEntry': 'Heropenen toestaan',
    'SplitOutstandingItemsPerInvoice': 'Automatisch splitsen',
    'PeriodDateCheck': 'Periode datum controle',
    'Open': 'Openingsbalans',
    'Close': 'Eindbalans',
    'Debit': 'Debet balans',
    'Credit': 'Credit balans',
    'InvoiceNumber': 'Factuur',
    'Amount': 'Bedrag',
    'JournalCode': 'Dagboek',
    'AmountFC': 'Bedrag',
    'YourRef': 'Uw referentie',
    'Date': 'Datum',
    'Quantity': 'Aantal',
    'VATCode': 'BTW code',
    'SettingName': 'Instelling',
    'Value': 'Waarde',
}

API_TRANSLATIONS = {
    'accounts': 'Relatie',
    'glaccounts': 'Grootboekrekening',
    'journals': 'Dagboek',
    'vats': 'BTW Code',
    'paymentconditions': 'Betalingsconditie',
    'contacts': 'Contact',
    'itemgroups': 'Artikelgroep',
    'items': 'Artikel',
    'financialperiods': 'Boekjaar/Periode',
    'settings': 'Instelling',
    'balancebyyear': 'Grootboekrekening',
    'receivableslist': 'Debiteur',
    'payableslist': 'Crediteur',
    'transactionlines60': 'Inkoopfactuur',
    'transactionlines70': 'Verkoopfactuur',
    'transactionlines90': 'Memoriaal boeking',
    'transactionlines20': 'Bankboeking',
}
