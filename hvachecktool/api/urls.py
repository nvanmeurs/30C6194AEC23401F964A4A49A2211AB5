from django.conf.urls import patterns, url
from rest_framework.routers import SimpleRouter

from api import views

# http://www.django-rest-framework.org/api-guide/routers/#simplerouter
router = SimpleRouter()
router.register(r'groups', views.GroupViewSet)
router.register(r'students', views.StudentViewSet)

urlpatterns = patterns('',
                       url(r'^current-me/$', views.CurrentMe.as_view(), name='current_me'),
                       url(r'^debug/$', views.DebugApi.as_view(), name='debug_api'),
                       url(r'^groups/(?P<pk>\w+)/students/$',
                           views.StudentViewSet.as_view({'get': 'list'}), name='group-students'),
                       )

urlpatterns += router.urls