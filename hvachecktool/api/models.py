from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from api.settings import ANSWER_DESCRIPTIONS, API_TRANSLATIONS
from api_client.models import Course
from api.util import add_leading_zero
from allauth.socialaccount.models import SocialAccount


@python_2_unicode_compatible
class Assignment(models.Model):
    SHOULD_BE_CREATED = 0
    SHOULD_BE_REMOVED = 1
    SHOULD_BE_RENAMED = 2
    FIN_YEAR_SHOULD_BE_CREATED = 3
    CHECK_TYPE_CHOICES = (
        (SHOULD_BE_CREATED, 'should be created'),
        (SHOULD_BE_REMOVED, 'should be removed'),
        (SHOULD_BE_RENAMED, 'should be renamed'),
        (FIN_YEAR_SHOULD_BE_CREATED, 'financial year should be created'),
    )
    topic_number = models.IntegerField()
    assignment_number = models.IntegerField()
    subassignment_number = models.IntegerField()
    description = models.CharField(max_length=255)
    check_type = models.IntegerField(choices=CHECK_TYPE_CHOICES)
    course = models.ForeignKey(Course, related_name='assignments')

    @property
    def topic(self):
        return 't%s' % add_leading_zero(self.topic_number)

    @property
    def assignment(self):
        return 'a%s_%s' % (add_leading_zero(self.topic_number), add_leading_zero(self.assignment_number))

    @property
    def subassignment(self):
        return '%s_%s' % (self.assignment, add_leading_zero(self.subassignment_number))

    def __str__(self):
        return '%s' % self.description

    class Meta:
        ordering = ['topic_number', 'assignment_number', 'subassignment_number']


@python_2_unicode_compatible
class Answer(models.Model):
    answer_number = models.IntegerField()
    api_name = models.CharField(max_length=60)
    api_field = models.CharField(max_length=60)
    correct_answer = models.CharField(max_length=255)
    answer_suffix = models.CharField(max_length=60, blank=True, null=True)
    assignment = models.ForeignKey(Assignment, related_name='answers')

    @property
    def translated_api_field(self):
        if self.api_field[:10] == 'VATBoxLink':
            api_field = 'VATBoxLink'
        else:
            api_field = self.api_field

        if api_field in ANSWER_DESCRIPTIONS:
            return ANSWER_DESCRIPTIONS[api_field]
        else:
            return api_field

    @property
    def translated_api_name(self):
        if self.api_name in API_TRANSLATIONS:
            return API_TRANSLATIONS[self.api_name]
        else:
            return self.api_name

    @property
    def answer(self):
        return '%s_%s' % (self.assignment.subassignment, add_leading_zero(self.answer_number))

    def __str__(self):
        return '%s %s %s' % (self.answer, self.api_name, self.api_field)

    class Meta:
        ordering = ['assignment', 'answer_number']


class FlaggedUser(models.Model):
    student_checked = models.ForeignKey(SocialAccount, related_name='flagged_student')
    checked_by = models.ForeignKey(SocialAccount, related_name='flagged_checker')
    flagged_on = models.DateTimeField(auto_now=True, auto_now_add=True)