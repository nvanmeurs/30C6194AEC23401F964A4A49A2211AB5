# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('socialaccount', '__first__'),
        ('api', '0003_auto_20141223_1033'),
    ]

    operations = [
        migrations.CreateModel(
            name='FlaggedUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('flagged_on', models.DateTimeField(auto_now=True, auto_now_add=True)),
                ('checked_by', models.ForeignKey(related_name='flagged_checker', to='socialaccount.SocialAccount')),
                ('student_checked', models.ForeignKey(related_name='flagged_student', to='socialaccount.SocialAccount')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
