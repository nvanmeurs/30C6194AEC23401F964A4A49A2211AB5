# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='answer',
            options={'ordering': ['assignment', 'answer_number']},
        ),
        migrations.AlterModelOptions(
            name='assignment',
            options={'ordering': ['topic_number', 'assignment_number', 'subassignment_number']},
        ),
    ]
