# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('api_client', '0004_auto_20141211_1339'),
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('answer_number', models.IntegerField()),
                ('api_name', models.CharField(max_length=60)),
                ('api_field', models.CharField(max_length=60)),
                ('correct_answer', models.CharField(max_length=255)),
                ('answer_suffix', models.CharField(max_length=60, null=True, blank=True)),
            ],
            options={
                'ordering': ['answer_number'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Assignment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('topic_number', models.IntegerField()),
                ('assignment_number', models.IntegerField()),
                ('subassignment_number', models.IntegerField()),
                ('description', models.CharField(max_length=255)),
                ('check_type', models.IntegerField(
                    choices=[(0, b'should be created'), (1, b'should be removed'), (2, b'should be renamed')])),
                ('course', models.ForeignKey(related_name='assignments', to='api_client.Course')),
            ],
            options={
                'ordering': ['subassignment_number'],
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='answer',
            name='assignment',
            field=models.ForeignKey(related_name='answers', to='api.Assignment'),
            preserve_default=True,
        ),
    ]
