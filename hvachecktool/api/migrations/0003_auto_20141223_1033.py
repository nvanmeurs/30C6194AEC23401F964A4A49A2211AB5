# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('api', '0002_auto_20141222_1040'),
    ]

    operations = [
        migrations.AlterField(
            model_name='assignment',
            name='check_type',
            field=models.IntegerField(
                choices=[(0, b'should be created'), (1, b'should be removed'), (2, b'should be renamed'),
                         (3, b'financial year should be created')]),
            preserve_default=True,
        ),
    ]
