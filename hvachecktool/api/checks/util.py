def to_lower_and_strip_spaces(value):
    return value.lower().replace(' ', '')