SHOULD_BE_CREATED_TEMPLATE_FIRST = "%(api_name)s '%(correct_answer)s' is %(suffix)s aangemaakt."
SHOULD_BE_CREATED_TEMPLATE_OTHER = "%(api_field)s '%(student_answer)s' van %(api_name)s '%(first_answer_value)s' is"
SHOULD_BE_REMOVED_TEMPLATE = "%(api_name)s '%(correct_answer)s' is %(suffix)s verwijderd."
SHOULD_BE_RENAMED_TEMPLATE = "%(api_name)s '%(second_answer)s' is correct vernummerd naar '%(first_answer)s'."


def _should_be_created_first(first_answer, suffix):
    return SHOULD_BE_CREATED_TEMPLATE_FIRST % {
        'api_name': first_answer.translated_api_name,
        'correct_answer': first_answer.correct_answer.strip(),
        'suffix': suffix
    }


def _should_be_created_other(api_field, student_answer, api_name, first_answer_value):
    return SHOULD_BE_CREATED_TEMPLATE_OTHER % {
        'api_field': api_field,
        'student_answer': student_answer.strip(),
        'api_name': api_name,
        'first_answer_value': first_answer_value.strip()
    }


def _should_be_removed(api_name, correct_answer, suffix):
    return SHOULD_BE_REMOVED_TEMPLATE % {
        'api_name': api_name,
        'correct_answer': correct_answer.strip(),
        'suffix': suffix
    }


def first_answer_created(first_answer):
    return _should_be_created_first(first_answer, 'correct')


def first_answer_not_created(first_answer):
    return _should_be_created_first(first_answer, 'niet')


def other_answer_created(api_field, student_answer, api_name, first_answer_value):
    suffix = 'correct aangemaakt.'
    message = _should_be_created_other(api_field, student_answer, api_name, first_answer_value)

    return "%s %s" % (message, suffix)


def other_answer_not_created(api_field, student_answer, api_name, first_answer_value, correct_answer):
    suffix = "niet gelijk aan verwachte %(api_field)s '%(correct_answer)s'." % {
        'api_field': api_field,
        'correct_answer': correct_answer.strip()
    }
    message = _should_be_created_other(api_field, student_answer, api_name, first_answer_value)

    return "%s %s" % (message, suffix)


def answer_removed(api_name, correct_answer):
    return _should_be_removed(api_name, correct_answer, 'succesvol')


def answer_not_removed(api_name, correct_answer):
    return _should_be_removed(api_name, correct_answer, 'niet')


def answers_renamed(api_name, first_answer, second_answer):
    return SHOULD_BE_RENAMED_TEMPLATE % {
        'api_name': api_name,
        'first_answer': first_answer.strip(),
        'second_answer': second_answer.strip()
    }