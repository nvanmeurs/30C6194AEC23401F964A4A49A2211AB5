from collections import OrderedDict
from api.checks.exceptions import InvalidCheckTypeException
from api.checks.messages import other_answer_created, other_answer_not_created, first_answer_created, \
    first_answer_not_created, answer_removed, answer_not_removed, answers_renamed
from api.checks.util import to_lower_and_strip_spaces as _c
from api.models import Assignment
from api_client.endpoints import API_ENDPOINTS


class Checker(object):
    def __init__(self, assignments, all_student_answers):
        self.assignments = assignments
        self.all_student_answers = all_student_answers
        self.check_results = OrderedDict()

    def run(self):
        for assignment in self.assignments:
            assignment_checker = AssignmentChecker(assignment, self.all_student_answers)
            assignment_checker.check()

            if assignment_checker.is_correct is not None:
                if assignment.topic not in self.check_results:
                    self.check_results[assignment.topic] = OrderedDict()

                if assignment.assignment not in self.check_results[assignment.topic]:
                    self.check_results[assignment.topic][assignment.assignment] = OrderedDict()

                self.check_results[assignment.topic][assignment.assignment][assignment.subassignment] = \
                    assignment_checker.check_results


class AssignmentChecker(object):
    def __init__(self, assignment, student_answers):
        self.assignment = assignment
        self._related_answers = self.assignment.answers.all()
        self.first_answer = self._related_answers[0]
        self.other_answers = self._related_answers[1:]
        self.student_answers = student_answers
        self.api_name = self.first_answer.api_name
        self.lookup_key = self._get_lookup_key()
        self.is_correct = None
        self.check_results = OrderedDict()

    def _get_lookup_key(self):
        lookup_fields = API_ENDPOINTS[self.api_name].lookup_fields

        if len(lookup_fields) == 1:
            return self.first_answer.correct_answer

        if len(lookup_fields) > 1:
            lookup_key = ''

            api_field_correct_answer = {answer.api_field: answer.correct_answer for answer in self._related_answers}

            for lookup_field in lookup_fields:
                lookup_key += '%s__' % api_field_correct_answer[lookup_field]

            # Delete trailing underscores.
            lookup_key = lookup_key[:-2]

            return lookup_key

    def check(self):
        # Check if there are any student answers for the current API, else do nothing.
        if self.first_answer.api_name in self.student_answers:

            checks = {
                Assignment.SHOULD_BE_CREATED: self.check_if_assignment_is_created,
                Assignment.SHOULD_BE_REMOVED: self.check_if_assignment_is_removed,
                Assignment.SHOULD_BE_RENAMED: self.check_if_assignment_is_renamed,
                Assignment.FIN_YEAR_SHOULD_BE_CREATED: self.check_if_financial_year_is_created,
            }

            if self.assignment.check_type in checks:
                checks[self.assignment.check_type]()
                self.serialize_assignment()
            else:
                raise InvalidCheckTypeException(
                    'Check type %r is not a valid check type.' % self.assignment.check_type)

    def serialize_assignment(self):
        self.check_results['desc'] = self.assignment.description

    def serialize_answer(self, correct_answer, is_correct, message):
        self.check_results[correct_answer.answer] = OrderedDict(
            desc=correct_answer.translated_api_field,
            result=1 if is_correct else 0,
            message=message
        )

    def check_if_assignment_is_created(self):
        """
        An assignment that should be created is correct when
        the first related answer occurs in the list of student answers and
        the student answer is equal to the correct answer.

        An assignment that should be created can have one or more related answers.
        An Answer related to the assignment is correct if it occurs in the list of student answers and
        the student answer is equal to the correct answer.
        """
        self.is_correct = self.lookup_key in self.student_answers[self.api_name] \
                          and self._answer_is_created(self.first_answer)

        if self.is_correct:
            message = first_answer_created(self.first_answer)
        else:
            message = first_answer_not_created(self.first_answer)

        self.serialize_answer(self.first_answer, self.is_correct, message)

        if self.is_correct and self.other_answers:
            for answer in self.other_answers:
                if answer.api_name not in self.student_answers:
                    continue

                answer_is_correct = self._answer_is_created(answer)

                if answer_is_correct:
                    answer_message = other_answer_created(
                        answer.translated_api_field,
                        self.student_answers[self.api_name][self.lookup_key][answer.api_field],
                        answer.translated_api_name,
                        self.lookup_key
                    )
                else:
                    student_answer = ''

                    if answer.api_field in self.student_answers[self.api_name]:
                        student_answer = self.student_answers[self.api_name][self.lookup_key][answer.api_field]

                    answer_message = other_answer_not_created(
                        answer.translated_api_field,
                        student_answer,
                        answer.translated_api_name,
                        self.lookup_key,
                        answer.correct_answer
                    )

                self.serialize_answer(answer, answer_is_correct, message=answer_message)

    def check_if_assignment_is_removed(self):
        """
         An assignment that should be removed is correct when
         the first related answer does not occur in the list of student answers.

         Other related answers will be ignored.
        """
        self.is_correct = self.lookup_key not in self.student_answers[self.api_name]

        if self.is_correct:
            message = answer_removed(
                self.first_answer.translated_api_name,
                self.first_answer.correct_answer
            )
        else:
            message = answer_not_removed(
                self.first_answer.translated_api_name,
                self.first_answer.correct_answer
            )

        self.serialize_answer(self.first_answer, self.is_correct, message)

    def check_if_assignment_is_renamed(self):
        """
         An assignment that should be renamed is correct when
         the first related answer is created (exists and correct) and
         the second related answer is removed (does not occur in student answers).

         Other related answers will be ignored.
         Will raise an invalid check type exception when the assignment has less than two related answers.
        """
        self.is_correct = False
        answer_is_correct = False

        if self.lookup_key in self.student_answers[self.api_name]:

            if len(self.other_answers) < 1:
                InvalidCheckTypeException('Assignment with check type %r should have two related answers. '
                                          'Not more, not less. %d answers given.'
                                          % (self.assignment.check_type, len(self.other_answers) + 1))

            second_answer = self.other_answers[0]
            self.is_correct = self._answer_is_created(self.first_answer)

            answer_is_correct = second_answer.correct_answer not in self.student_answers[second_answer.api_name]

        both_correct = answer_is_correct and self.is_correct

        if both_correct:
            message = answers_renamed(
                self.first_answer.translated_api_name,
                self.first_answer.correct_answer,
                second_answer.correct_answer
            )
            self.serialize_answer(self.first_answer, self.is_correct, message)
        else:
            if self.is_correct and not both_correct:
                message = first_answer_created(self.first_answer)

                if answer_is_correct:
                    answer_message = answer_removed(
                        second_answer.translated_api_name,
                        second_answer.correct_answer
                    )
                else:
                    answer_message = answer_not_removed(
                        second_answer.translated_api_name,
                        second_answer.correct_answer
                    )

                self.serialize_answer(second_answer, answer_is_correct, message=answer_message)
            else:
                message = first_answer_not_created(self.first_answer)

            self.serialize_answer(self.first_answer, self.is_correct, message)

    def check_if_financial_year_is_created(self):
        """
        A financial year that should be created is correct when
        _all_ of the related answer occur in the list of student answers and
        _all_ of the student answers are equal to the correct answers.
        """
        incorrect_answers = []

        for answer in self._related_answers:
            if not (
                    # Assignment occurs in student answers.
                    answer.correct_answer in self.student_answers[self.api_name]
                    and
                    # Answer occurs in student answers.
                            answer.api_field in self.student_answers[answer.api_name][answer.correct_answer]
                    and
                    # Student answer is correct.
                            _c(answer.correct_answer) ==
                            _c(self.student_answers[answer.api_name][answer.correct_answer][answer.api_field])
            ):
                incorrect_answers.append(answer)

        if not incorrect_answers:
            self.is_correct = True

            message = "%(api_name)s '%(first_period)s' tot en met '%(last_period)s' zijn correct aangemaakt." % {
                'api_name': self.first_answer.translated_api_name,
                'first_period': self.first_answer.correct_answer,
                'last_period': self._related_answers[self._related_answers.count() - 1].correct_answer,
            }

            self.serialize_answer(self.first_answer, self.is_correct, message)
        else:
            self.is_correct = False

            for incorrect_answer in incorrect_answers:
                self.serialize_answer(incorrect_answer, self.is_correct, first_answer_not_created(incorrect_answer))

    def _answer_is_created(self, answer):
        return answer.api_field in self.student_answers[self.api_name][self.lookup_key] \
               and _c(answer.correct_answer) == \
                   _c(self.student_answers[self.api_name][self.lookup_key][answer.api_field])