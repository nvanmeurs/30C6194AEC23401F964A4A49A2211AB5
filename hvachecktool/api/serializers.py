from rest_framework import serializers

from api_client.models import Student, Group


class GroupModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('id', 'description')


class StudentModelSerializer(serializers.ModelSerializer):
    group = serializers.PrimaryKeyRelatedField()
    name = serializers.SerializerMethodField(method_name='get_name')
    exact_guid = serializers.SerializerMethodField(method_name='get_exact_guid')

    def get_name(self, obj):
        guid = obj.account.uid

        if 'names' not in self.context or guid not in self.context['names']:
            return None

        name = self.context['names'][guid]

        return name.strip()

    def get_exact_guid(self, obj):
        return obj.account.uid

    class Meta:
        model = Student
        fields = ('id', 'division', 'group', 'name', 'exact_guid')


class CurrentMeSerializer(serializers.Serializer):
    exact_uid = serializers.CharField(max_length=255)
    username = serializers.CharField(max_length=255)
    current_division = serializers.CharField(max_length=255)
    school_name = serializers.CharField(max_length=255)
    full_name = serializers.CharField(max_length=255)
    role = serializers.CharField(max_length=255)
