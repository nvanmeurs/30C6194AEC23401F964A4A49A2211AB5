def add_leading_zero(value):
    if int(value) < 10:
        return '0%s' % value
    else:
        return '%s' % value