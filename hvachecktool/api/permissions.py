from rest_framework.permissions import BasePermission

from api_client.auth import Auth, UserIsNotAnApiUserException, UserIsNotAuthenticatedException


class IsAuthenticatedAndHasValidAccessToken(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        try:
            auth = Auth(request.user)
            if auth.tokens.expires_within_three_minutes_from_now():
                auth.tokens.refresh()
            return True
        except (UserIsNotAnApiUserException, UserIsNotAuthenticatedException):
            return False