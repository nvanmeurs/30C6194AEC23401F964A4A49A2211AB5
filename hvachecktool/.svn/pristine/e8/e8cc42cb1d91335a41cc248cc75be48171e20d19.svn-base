from datetime import timedelta
import logging
from allauth.socialaccount.models import SocialToken

from django.contrib.auth.models import AnonymousUser
from django.utils import timezone

from api_client.async import AsyncClient
from api_client.endpoints import Tokens
from api_client.loggers import log_successful_call


class UserIsNotAnApiUserException(Exception):
    pass


class UserIsNotAuthenticatedException(Exception):
    pass


class ExactAuthenticationError(Exception):
    pass


class Auth(object):
    def __init__(self, user):
        if isinstance(user, AnonymousUser):
            raise UserIsNotAuthenticatedException('You are not authenticated.')

        tokens = SocialToken.objects\
            .select_related('account__user_id', 'app__client_id', 'app__secret', 'app__key')\
            .filter(account__user_id=user.id).first()

        if not tokens:
            raise UserIsNotAnApiUserException(
                'This user is not an exact user and therefor does not have any auth tokens.')

        self.tokens = AuthTokens(tokens)



class AuthTokens(object):
    def __init__(self, tokens):
        self._tokens = tokens
        self._client_id = self._tokens.app.client_id
        self._client_secret = self._tokens.app.secret
        self._partner_key = self._tokens.app.key

    @property
    def access_token(self):
        if self.expires_within_three_minutes_from_now():
            self.refresh()
        return self._tokens.token

    @property
    def refresh_token(self):
        return self._tokens.token_secret

    @property
    def expires_at(self):
        return self._tokens.expires_at

    @property
    def client_id(self):
        return self._client_id

    @property
    def client_secret(self):
        return self._client_secret

    @property
    def partner_key(self):
        return self._partner_key


    def expires_within_three_minutes_from_now(self):
        three_minutes_from_now = timezone.now() + timedelta(minutes=3)

        return self.expires_at < three_minutes_from_now

    def refresh(self):
        # Initialize the asynchronous HTTP client.
        client = AsyncClient()

        # Add the tokens endpoint to the client.
        client.add_call(
            Tokens(client_id=self.client_id, client_secret=self.client_secret, refresh_token=self.refresh_token))

        # Start the client and send the requests.
        client.start()

        # Get the tokens endpoint results from the client.
        results = client.results['tokens']

        if 'error' in results:
            logger = logging.getLogger(__name__)
            logger.setLevel(logging.DEBUG)
            handler = logging.FileHandler('auth.log')
            handler.setLevel(logging.DEBUG)
            formatter = logging.Formatter('[%(asctime)s:%(levelname)s] %(message)s')
            handler.setFormatter(formatter)
            logger.addHandler(handler)

            logger.debug('___________response_________')
            logger.debug('URL: %r' % results.url)
            logger.debug('Status code: %r' % results.status_code)
            logger.debug('Headers: %r' % results.headers)
            logger.debug('Content: %r' % results.content)
            logger.debug('____________________________')

            raise ExactAuthenticationError('Exact authentication error: %s' % results['error'])

        # Update access token.
        self._tokens.token = results['access_token']

        # Update refresh token.
        self._tokens.token_secret = results['refresh_token']

        # Calculate when the new access_token will be expired.
        expires_at = timezone.now() + timedelta(seconds=int(results['expires_in']))

        # Update the time the access token will expire.
        self._tokens.expires_at = expires_at

        # Update the record in the database that stores the user's tokens.
        self._tokens.save()