from api.checks.checker import Checker
from api_client.async import AsyncClient
from api_client import endpoints
from api.models import Assignment


def get_check_results(access_token, partner_key, division, current_financial_year=2014, next_financial_year=2015):
    client = AsyncClient()

    for endpoint in [
        endpoints.GLAccounts(access_token=access_token, division=division),
        endpoints.GLAccountsById(access_token=access_token, division=division),
        endpoints.Journals(access_token=access_token, division=division),
        endpoints.VATs(access_token=access_token, division=division, partner_key=partner_key),
        endpoints.PaymentConditions(access_token=access_token, division=division),
        endpoints.Accounts(access_token=access_token, division=division),
        endpoints.Contacts(access_token=access_token, division=division),
        endpoints.ItemGroups(access_token=access_token, division=division),
        endpoints.Items(access_token=access_token, division=division),
        endpoints.BalanceByYear(access_token=access_token, division=division,
                                current_financial_year=current_financial_year),
        endpoints.FinancialPeriods(access_token=access_token, division=division,
                                   next_financial_year=next_financial_year),
        endpoints.PayablesList(access_token=access_token, division=division),
        endpoints.ReceivablesList(access_token=access_token, division=division),
        endpoints.Settings(access_token=access_token, division=division, partner_key=partner_key),
        endpoints.TransactionLines60(access_token=access_token, division=division),
        endpoints.TransactionLines70(access_token=access_token, division=division),
        endpoints.TransactionLines90(access_token=access_token, division=division),
        endpoints.TransactionLines20(access_token=access_token, division=division),
    ]:
        client.add_call(endpoint)

    client.start()

    student_answers = client.results

    for account_name, account in student_answers.accounts.iteritems():
        glaccount_id = account['GLAccountPurchase']
        if glaccount_id in student_answers.glaccounts:
            account['GLAccountPurchase'] = student_answers.glaccounts[glaccount_id]['Code']
            del student_answers.glaccounts[glaccount_id]

    # Get the correct answers from the database.
    all_correct_answers = None

    # Get the assignments (parent) from the set of correct answers,
    assignments = Assignment.objects.all().prefetch_related('answers')

    checker = Checker(assignments, student_answers.to_dict())
    checker.run()

    return checker.check_results