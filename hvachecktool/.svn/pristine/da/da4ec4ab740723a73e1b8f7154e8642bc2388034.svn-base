from addict import Dict

from api_client.provider import ExactOAuth2Adapter
from api_client.util import build_query_string, build_headers, decode_json_content, filter_api_value, decode_xml_content
from api_client.loggers import log_successful_call, log_failed_call, logger


class BaseEndpoint(object):
    base_url = 'https://start.exactonline.nl'
    uri = ''
    content_type = 'json'
    extra_headers = None
    params = None
    data = None
    method = 'get'
    name = None

    def __init__(self, access_token=None):
        self.access_token = access_token

    @property
    def query_string(self):
        return build_query_string(self.params)

    @property
    def url(self):
        return '%s%s%s' % (self.base_url, self.uri, self.query_string)

    @property
    def headers(self):
        return build_headers(
            content_type=self.content_type,
            access_token=self.access_token,
            extra_headers=self.extra_headers
        )

    def on_success(self, **kwargs):
        log_successful_call(**kwargs)

    def on_fail(self, **kwargs):
        log_failed_call(**kwargs)


class ParamsMixin(object):
    select = None
    filter = None

    @property
    def query_string(self):
        if self.params is None:
            self.params = Dict()

        if self.select is not None:
            self.params['$select'] = ",".join(self.select)

        if self.filter is not None:
            self.params['$filter'] = self.filter

        return build_query_string(self.params)


class GenericJsonEndpoint(ParamsMixin, BaseEndpoint):
    def on_success(self, async_client, response, **kwargs):
        super(GenericJsonEndpoint, self).on_success(response=response, async_client=async_client, **kwargs)
        decoded_content = decode_json_content(response)
        async_client.results[self.name] = decoded_content["d"]["results"]


class GenericIndexedJsonEndpoint(ParamsMixin, BaseEndpoint):
    lookup_fields = None

    def __init__(self, division=None, **kwargs):
        super(GenericIndexedJsonEndpoint, self).__init__(**kwargs)
        if division is not None:
            self.uri = self.uri.format(division=division)

    def on_success(self, response, async_client, **kwargs):
        super(GenericIndexedJsonEndpoint, self).on_success(response=response, async_client=async_client, **kwargs)

        # Decode the JSON content of the response.
        decoded_content = decode_json_content(response)

        # Index the decoded content by its key.
        results = {}

        for assignment in decoded_content["d"]["results"]:

            # Convert all values in the item to an unicode string.
            for field in assignment:
                assignment[field] = filter_api_value(assignment[field])

            # Add assignment to the async_client results for later retrieval.
            for lookup_field in self.lookup_fields:
                async_client.results[self.name][assignment[lookup_field]] = assignment


class GenericIndexedXmlEndpoint(BaseEndpoint):
    lookup_fields = None
    topic = None
    uri = '/docs/XMLDownload.aspx/'
    content_type = 'xml'

    def __init__(self, division, partner_key, **kwargs):
        super(GenericIndexedXmlEndpoint, self).__init__(**kwargs)
        self.params = Dict()
        self.params['Topic'] = self.topic
        self.params['PartnerKey'] = partner_key
        self.params['_Division_'] = division
        self.decoded_content = None

    def on_success(self, response, async_client, **kwargs):
        super(GenericIndexedXmlEndpoint, self).on_success(response=response, async_client=async_client, **kwargs)
        self.decoded_content = decode_xml_content(response)


class CurrentMe(GenericJsonEndpoint):
    name = 'current_me'
    uri = '/api/v1/current/Me/'


class GLAccounts(GenericIndexedJsonEndpoint):
    name = 'glaccounts'
    lookup_fields = ['Code']
    uri = '/api/v1/{division}/financial/GLAccounts/'
    select = ['ID', 'Code', 'Description']


class GLAccountsById(GLAccounts):
    lookup_fields = ['ID']
    select = ['ID', 'Code']


class Journals(GenericIndexedJsonEndpoint):
    name = 'journals'
    lookup_fields = ['Code']
    uri = '/api/v1/{division}/financial/Journals/'
    select = ['Code', 'Description', 'Type', 'GLAccountCode']


class VATs(GenericIndexedXmlEndpoint):
    name = 'vats'
    lookup_fields = ['Code']
    topic = 'VATs'

    def on_success(self, response, async_client, **kwargs):
        super(VATs, self).on_success(response=response, async_client=async_client, **kwargs)
        for vat in self.decoded_content['eExact']['VATs']['VAT']:
            item = {
                'Code': vat['@code'],
                'Type': vat['@type']
            }

            for field in ['Description', 'Percentage']:
                item[field] = vat[field]

            for field in ['GLToPay', 'GLToClaim']:
                if field in vat:
                    item['%sCode' % field] = vat[field]['@code']

            if 'VATBoxLink' in vat:
                for vat_box_link in vat['VATBoxLink']:
                    vat_box_type = vat_box_link['@type']

                    if vat_box_type in ['I', 'V']:
                        vat_box_code = vat_box_link['VATBox']['@code']
                        item['VATBoxLink%s%s' % (vat_box_type, vat_box_code)] = vat_box_code

            for lookup_field in self.lookup_fields:
                async_client.results[self.name][item[lookup_field]] = item


class PaymentConditions(GenericIndexedJsonEndpoint):
    name = 'paymentconditions'
    lookup_fields = ['Code']
    uri = '/api/v1/{division}/cashflow/PaymentConditions/'
    select = ['Code', 'Description', 'PaymentDays']


class Accounts(GenericIndexedJsonEndpoint):
    name = 'accounts'
    lookup_fields = ['Name']
    uri = '/api/v1/{division}/crm/Accounts/'
    select = [
        'ID', 'Name', 'AddressLine1', 'City', 'Status', 'IsSupplier',
        'VATNumber', 'PaymentConditionPurchase', 'PurchaseVATCode', 'GLAccountPurchase',
        'Postcode'
    ]


class Contacts(GenericIndexedJsonEndpoint):
    name = 'contacts'
    lookup_fields = ['FullName']
    uri = '/api/v1/{division}/read/crm/Contacts/'
    select = ['Account', 'AccountName', 'FullName']


class ItemGroups(GenericIndexedJsonEndpoint):
    name = 'itemgroups'
    lookup_fields = ['Code']
    uri = '/api/v1/{division}/logistics/ItemGroups/'
    select = ['Code', 'Description', 'GLRevenueCode', 'GLRevenueDescription']


class Items(GenericIndexedJsonEndpoint):
    name = 'items'
    lookup_fields = ['Code']
    uri = '/api/v1/{division}/read/logistics/Items/'
    select = [
        'Code', 'ItemGroupCode', 'Description', 'SalesPrice', 'SalesVatCode',
        'UnitDescription', 'IsFractionAllowedItem', 'StartDate'
    ]


class BalanceByYear(GenericIndexedJsonEndpoint):
    name = 'balancebyyear'
    lookup_fields = ['GLAccountCode']
    uri = '/api/v1/{division}/balance/BalanceByYear/'
    select = ['GLAccountCode', 'Open', 'Debit', 'Credit', 'Close']
    filter = 'FinancialYear%20eq%20{current_financial_year}'

    def __init__(self, current_financial_year, **kwargs):
        super(BalanceByYear, self).__init__(**kwargs)
        self.filter = self.filter.format(current_financial_year=current_financial_year)


class FinancialPeriods(GenericIndexedJsonEndpoint):
    name = 'financialperiods'
    lookup_fields = ['FinPeriod']
    uri = '/api/v1/{division}/financial/FinancialPeriods/'
    select = ['FinPeriod']
    filter = 'FinYear%20eq%20{next_financial_year}'

    def __init__(self, next_financial_year, **kwargs):
        super(FinancialPeriods, self).__init__(**kwargs)
        self.filter = self.filter.format(next_financial_year=next_financial_year)


class PayablesList(GenericIndexedJsonEndpoint):
    name = 'payableslist'
    lookup_fields = ['InvoiceNumber']
    uri = '/api/v1/{division}/read/financial/PayablesList/'
    select = ['InvoiceNumber', 'Amount', 'AccountName', 'JournalCode']


class ReceivablesList(GenericIndexedJsonEndpoint):
    name = 'receivableslist'
    lookup_fields = ['InvoiceNumber']
    uri = '/api/v1/{division}/read/financial/ReceivablesList/'
    select = ['InvoiceNumber', 'Amount', 'AccountName', 'JournalCode']


class Settings(GenericIndexedXmlEndpoint):
    name = 'settings'
    lookup_fields = ['SettingName']
    topic = 'Settings'

    def on_success(self, response, async_client, **kwargs):
        super(Settings, self).on_success(response=response, async_client=async_client, **kwargs)
        for setting in self.decoded_content['eExact']['Settings']['Setting']:
            item = {
                'SettingName': setting['@SettingName'],
                'Value': setting['@Value']
            }

            for lookup_field in self.lookup_fields:
                async_client.results[self.name][item[lookup_field]] = item


class Divisions(GenericIndexedJsonEndpoint):
    name = 'divisions'
    lookup_fields = ['Code']
    uri = '/api/v1/{student_division}/hrm/Divisions/'
    select = ['Code', 'Description']
    filter = 'Code%20eq%20{student_division}'

    def __init__(self, student_division, **kwargs):
        super(Divisions, self).__init__(**kwargs)
        self.uri = self.uri.format(student_division=student_division)
        self.filter = self.filter.format(student_division=student_division)

    def on_success(self, response, async_client, **kwargs):
        try:
            super(Divisions, self).on_success(response=response, async_client=async_client, **kwargs)
        except ValueError:
            # Do not trigger HTTP 500 error, when a division does not exist within exact.
            # Instead explicitly silence the error and write it to the log.
            logger.warning('Division %s does not exist within Exact.' % response.url)


class Tokens(GenericJsonEndpoint):
    name = 'tokens'
    method = 'post'

    def __init__(self, client_id, client_secret, refresh_token, **kwargs):
        super(Tokens, self).__init__(**kwargs)
        self.data = Dict()
        self.data.client_id = client_id
        self.data.client_secret = client_secret
        self.data.refresh_token = refresh_token
        self.data.grant_type = 'refresh_token'

    @property
    def url(self):
        return ExactOAuth2Adapter.access_token_url

    def on_success(self, response, async_client, **kwargs):
        log_successful_call(response=response, async_client=async_client, **kwargs)
        async_client.results[self.name] = decode_json_content(response)


class TransactionLines(GenericIndexedJsonEndpoint):
    name = 'transactionlines'
    uri = '/api/v1/{division}/financialtransaction/Transactions/'
    params = {
        '$expand': 'TransactionLines'
    }
    select = [
        'TransactionLines/AccountName',
        'TransactionLines/EntryNumber',
        'TransactionLines/LineNumber',
        'TransactionLines/AmountFC',
        'TransactionLines/AmountVATFC',
        'TransactionLines/Date',
        'TransactionLines/Description',
        'TransactionLines/FinancialPeriod',
        'TransactionLines/FinancialYear',
        'TransactionLines/GLAccountCode',
        'TransactionLines/PaymentReference',
        'TransactionLines/Quantity',
        'TransactionLines/VATCode',
        'TransactionLines/YourRef'
    ]
    filter = 'JournalCode%20eq%20%27{type}%27'
    type = None

    def __init__(self, **kwargs):
        super(TransactionLines, self).__init__(**kwargs)
        self.filter = self.filter.format(type=self.type) if self.type else None

    def on_success(self, response, async_client, **kwargs):
        log_successful_call(response=response, async_client=async_client, **kwargs)

        # Decode the JSON content of the response.
        decoded_content = decode_json_content(response)

        # Index the decoded content by its key.
        for transactions in decoded_content['d']['results']:
            for transactionline in transactions['TransactionLines']['results']:
                for field in transactionline:
                    transactionline[field] = filter_api_value(transactionline[field])

                lookup = '__'.join([transactionline[lookup_field] for lookup_field in self.lookup_fields])

                async_client.results[self.name][lookup] = transactionline


class TransactionLines60(TransactionLines):
    name = 'transactionlines60'
    type = '60'
    lookup_fields = ['YourRef', 'LineNumber']


class TransactionLines70(TransactionLines):
    name = 'transactionlines70'
    type = '70'
    lookup_fields = ['AccountName', 'Description']


class TransactionLines90(TransactionLines):
    name = 'transactionlines90'
    type = '90'
    lookup_fields = ['GLAccountCode', 'Description']


class Users(GenericIndexedJsonEndpoint):
    name = 'users'
    uri = '/api/v1/{student_division}/users/Users/'
    lookup_fields = ['UserID']
    select = ['UserID', 'FullName']
    filter = "UserID%20eq%20guid'{guid}'"

    def __init__(self, student_division, guid, **kwargs):
        super(Users, self).__init__(**kwargs)
        self.uri = self.uri.format(student_division=student_division)
        self.filter = self.filter.format(guid=guid)

    def on_success(self, response, async_client, **kwargs):
        try:
            super(Users, self).on_success(response=response, async_client=async_client, **kwargs)
        except ValueError:
            # Do not trigger HTTP 500 error, when a division does not exist within exact.
            # Instead explicitly silence the error and write it to the log.
            logger.warning('User %s does not exist within Exact.' % response.url)

API_ENDPOINTS = {}
API_ENDPOINTS['current_me'] = CurrentMe
API_ENDPOINTS['glaccounts'] = GLAccounts
API_ENDPOINTS['journals'] = Journals
API_ENDPOINTS['vats'] = VATs
API_ENDPOINTS['paymentconditions'] = PaymentConditions
API_ENDPOINTS['accounts'] = Accounts
API_ENDPOINTS['contacts'] = Contacts
API_ENDPOINTS['itemgroups'] = ItemGroups
API_ENDPOINTS['items'] = Items
API_ENDPOINTS['balancebyyear'] = BalanceByYear
API_ENDPOINTS['financialperiods'] = FinancialPeriods
API_ENDPOINTS['payableslist'] = PayablesList
API_ENDPOINTS['receivableslist'] = ReceivablesList
API_ENDPOINTS['settings'] = Settings
API_ENDPOINTS['divisions'] = Divisions
API_ENDPOINTS['tokens'] = Tokens
API_ENDPOINTS['transactionlines60'] = TransactionLines60
API_ENDPOINTS['transactionlines70'] = TransactionLines70
API_ENDPOINTS['transactionlines90'] = TransactionLines90
API_ENDPOINTS['users'] = Users
API_ENDPOINTS['glaccounts_by_id'] = GLAccountsById