import json

from addict import Dict
from rest_framework.views import APIView
from django.http import HttpResponse
from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.response import Response

from api_client.auth import Auth
from api_client.models import Group, Student
from api.serializers import GroupModelSerializer, StudentModelSerializer, CurrentMeSerializer
from api_client.async import AsyncClient
from api.permissions import IsAuthenticatedAndHasValidAccessToken
from api_client.util import get_access_token, get_partner_key
from api_client import endpoints
from api.models import Answer, Assignment
from api.checks.checker import Checker


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupModelSerializer
    permission_classes = [IsAuthenticatedAndHasValidAccessToken]

    @detail_route(methods=['post'])
    def add_students(self, request, pk=None):
        student_ids = json.loads(request.body)
        students = Student.objects.all()

        errors = {}

        for student_id in student_ids:
            student = students.filter(id=student_id)
            student = student[0] if student else None

            if not student:
                errors[student_id] = 'does not exist'
            elif student.group:
                errors[student_id] = 'already assigned to a group'
            else:
                student.group = self.get_object()
                student.save()

        return HttpResponse(json.dumps({'errors': errors}), content_type='application/json')


class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentModelSerializer
    permission_classes = [IsAuthenticatedAndHasValidAccessToken]
    _names = None
    # _divisions = None

    def get_serializer_context(self):
        return {
            'request': self.request,
            'format': self.format_kwarg,
            'view': self,
            # 'divisions': self._divisions
            'names': self._names
        }

    def get_queryset(self):
        group = self.request.GET.get('group', None)

        if group is not None:
            if group == 'null':
                return self.queryset.filter(group=None)

            return self.queryset.filter(group=group)

        return self.queryset

    def _get_names(self, students, access_token):
        for student in students:
            guid = student.account.uid

    # def _get_divisions(self, student_divisions, access_token):
    #     client = AsyncClient()
    #
    #     for student_division in student_divisions:
    #         client.add_call(endpoints.Divisions(access_token=access_token, student_division=student_division))
    #
    #     client.start()
    #
    #     self._divisions = client.results.divisions

    def list(self, request, *args, **kwargs):
        access_token = get_access_token(request.user)

        self.object_list = self.filter_queryset(self.get_queryset())

        # student_divisions = [student.division for student in self.object_list]
        #
        # self._get_divisions(student_divisions, access_token)

        serializer = self.get_serializer(self.object_list, many=True)

        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        access_token = get_access_token(request.user)

        self.object = self.get_object()
        # self._get_divisions([self.object.division], access_token)
        serializer = self.get_serializer(self.object)
        return Response(serializer.data)

    @detail_route(methods=['get'])
    def results(self, request, pk=None, **kwargs):
        self.object = self.get_object()
        access_token = get_access_token(request.user)
        partner_key = get_partner_key(request.user)

        # TODO: Get financial years from database.
        current_financial_year = 2014
        next_financial_year = 2015

        client = AsyncClient()

        for endpoint in [
            endpoints.GLAccounts(access_token=access_token, division=self.object.division),
            endpoints.Journals(access_token=access_token, division=self.object.division),
            endpoints.VATs(access_token=access_token, division=self.object.division, partner_key=partner_key),
            endpoints.PaymentConditions(access_token=access_token, division=self.object.division),
            endpoints.Accounts(access_token=access_token, division=self.object.division),
            endpoints.Contacts(access_token=access_token, division=self.object.division),
            endpoints.ItemGroups(access_token=access_token, division=self.object.division),
            endpoints.Items(access_token=access_token, division=self.object.division),
            endpoints.BalanceByYear(access_token=access_token, division=self.object.division,
                                    current_financial_year=current_financial_year),
            endpoints.FinancialPeriods(access_token=access_token, division=self.object.division,
                                       next_financial_year=next_financial_year),
            endpoints.PayablesList(access_token=access_token, division=self.object.division),
            endpoints.ReceivablesList(access_token=access_token, division=self.object.division),
            endpoints.Settings(access_token=access_token, division=self.object.division, partner_key=partner_key),
            endpoints.TransactionLines60(access_token=access_token, division=self.object.division),
        ]:
            client.add_call(endpoint)

        client.start()

        student_answers = client.results

        for account_name, account in student_answers.accounts.iteritems():
            glaccount_id = account['GLAccountPurchase']
            if glaccount_id in student_answers.glaccounts:
                account['GLAccountPurchase'] = student_answers.glaccounts[glaccount_id]['Code']
                del student_answers.glaccounts[glaccount_id]

        # Get the correct answers from the database.
        all_correct_answers = Answer.objects.all()

        # Get the assignments (parent) from the set of correct answers,
        assignments = Assignment.objects.all()

        checker = Checker(assignments, all_correct_answers, student_answers)
        checker.run()

        return HttpResponse(json.dumps([checker.check_results], sort_keys=True), content_type="application/json")


class CurrentMe(APIView):
    def get(self, request):
        access_token = get_access_token(request.user)
        client = AsyncClient()

        client.add_call(
            endpoints.CurrentMe(access_token=access_token)
        )

        client.start()

        results = client.results.current_me[0]
        print '%r' % results

        current_me = Dict()
        current_me.username = results['UserName']
        current_me.exact_uid = results['UserID']
        current_me.current_division = results['CurrentDivision']
        current_me.school_name = request.user.exactuser.school.schoolname

        print '%r' % current_me

        serializer = CurrentMeSerializer(current_me)

        return Response(serializer.data)


class DebugApi(APIView):
    def get(self, request):
        auth = Auth(request.user)

        debug_api = Dict()

        debug_api._meta.auth.old_access_token = auth.tokens.access_token
        auth.tokens.refresh()
        debug_api._meta.auth.new_access_token = auth.tokens.access_token

        access_token = auth.tokens.access_token
        partner_key = auth.tokens.partner_key

        debug_api._meta.auth.partner_key = partner_key

        division = request.GET.get('division', 692197)
        current_financial_year = request.GET.get('current_financial_year', 2014)
        next_financial_year = request.GET.get('next_financial_year', 2015)
        student_division = request.GET.get('student_division', 692197)
        entry_number = request.GET.get('entry_number', 15600002)

        debug_api._meta.params.division = division
        debug_api._meta.params.current_financial_year = current_financial_year
        debug_api._meta.params.next_financial_year = next_financial_year
        debug_api._meta.params.student_division = student_division
        debug_api._meta.params.entry_number = entry_number
        debug_api._meta.params._info = \
            'Change the params using a GET parameter, ex: /api/debug/?division=900&next_financial_year=2036'

        client = AsyncClient()

        for endpoint in [
            endpoints.CurrentMe(access_token=access_token),
            endpoints.GLAccounts(access_token=access_token, division=division),
            endpoints.Journals(access_token=access_token, division=division),
            endpoints.VATs(access_token=access_token, division=division, partner_key=partner_key),
            endpoints.PaymentConditions(access_token=access_token, division=division),
            endpoints.Accounts(access_token=access_token, division=division),
            endpoints.Contacts(access_token=access_token, division=division),
            endpoints.ItemGroups(access_token=access_token, division=division),
            endpoints.Items(access_token=access_token, division=division),
            endpoints.BalanceByYear(access_token=access_token, division=division,
                                    current_financial_year=current_financial_year),
            endpoints.FinancialPeriods(access_token=access_token, division=division,
                                       next_financial_year=next_financial_year),
            endpoints.PayablesList(access_token=access_token, division=division),
            endpoints.ReceivablesList(access_token=access_token, division=division),
            endpoints.Settings(access_token=access_token, division=division, partner_key=partner_key),
            endpoints.Divisions(access_token=access_token, student_division=student_division),
            endpoints.TransactionLines60(access_token=access_token, division=division),
            endpoints.TransactionLines70(access_token=access_token, division=division),
            endpoints.TransactionLines90(access_token=access_token, division=division),
        ]:
            client.add_call(endpoint)

        client.start()

        results = client.results

        debug_api.api_results = results

        return HttpResponse(json.dumps(debug_api, sort_keys=True), content_type='application/json')


def result_single(request, division):
    access_token = get_access_token(request.user)
    partner_key = get_partner_key(request.user)

    # TODO: Get financial years from database.
    current_financial_year = 2014
    next_financial_year = 2015

    client = AsyncClient()

    for endpoint in [
        endpoints.GLAccounts(access_token=access_token, division=division),
        endpoints.Journals(access_token=access_token, division=division),
        endpoints.VATs(access_token=access_token, division=division, partner_key=partner_key),
        endpoints.PaymentConditions(access_token=access_token, division=division),
        endpoints.Accounts(access_token=access_token, division=division),
        endpoints.Contacts(access_token=access_token, division=division),
        endpoints.ItemGroups(access_token=access_token, division=division),
        endpoints.Items(access_token=access_token, division=division),
        endpoints.BalanceByYear(access_token=access_token, division=division,
                                current_financial_year=current_financial_year),
        endpoints.FinancialPeriods(access_token=access_token, division=division,
                                   next_financial_year=next_financial_year),
        endpoints.PayablesList(access_token=access_token, division=division),
        endpoints.ReceivablesList(access_token=access_token, division=division),
        endpoints.Settings(access_token=access_token, division=division, partner_key=partner_key),
        endpoints.TransactionLines60(access_token=access_token, division=division),
    ]:
        client.add_call(endpoint)

    client.start()

    student_answers = client.results

    for account_name, account in student_answers.accounts.iteritems():
        glaccount_id = account['GLAccountPurchase']
        if glaccount_id in student_answers.glaccounts:
            account['GLAccountPurchase'] = student_answers.glaccounts[glaccount_id]['Code']
            del student_answers.glaccounts[glaccount_id]

    # Get the correct answers from the database.
    all_correct_answers = Answer.objects.all()

    # Get the assignments (parent) from the set of correct answers,
    assignments = Assignment.objects.all()

    checker = Checker(assignments, all_correct_answers, student_answers)
    checker.run()

    return HttpResponse(json.dumps([checker.check_results], sort_keys=True), content_type="application/json")