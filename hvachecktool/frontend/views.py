from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render

from api_client.views import logout
from api_client.auth import Auth, UserIsNotAnApiUserException
from api_client.models import Student


@login_required
def index(request):
    try:
        auth = Auth(request.user)
        if auth.tokens.expires_within_three_minutes_from_now():
            auth.tokens.refresh()
    except UserIsNotAnApiUserException:
        logout(request)
        return HttpResponseRedirect(reverse('exact_login'))

    try:
        user_id = request.user.id
        group_name = Group.objects.filter(user__id=user_id).only('name').first().name

        if group_name == 'teacher':
            return render(request, 'frontend/teacher.html')
        if group_name == 'student':
            student = Student.objects.select_related('account__user_id').filter(account__user_id=user_id).first()
            return render(request, 'frontend/student.html', {'student_id': student.id})

    except:
        return HttpResponse(
            'Uw account is geen leraar- of studenten account, neem a.u.b. contact op met de administrator.')