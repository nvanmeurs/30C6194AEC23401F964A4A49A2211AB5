<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" ng-app="app"ng-controller="app as appController" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" ng-app="app" ng-controller="app as appController" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" ng-app="app" ng-controller="app as appController" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html lang="en" ng-app="app" ng-controller="app as appController" class="no-js"> <!--<![endif]-->
    <head>
        <meta ng charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{appController.title}}</title>
        <meta name="description" content="">
        <link rel="stylesheet" href="client/bower_components/html5-boilerplate/css/normalize.css">
        <link rel="stylesheet" href="client/bower_components/html5-boilerplate/css/main.css">
        <link rel="stylesheet" href="client/css/app.css"/>
        <script src="client/bower_components/html5-boilerplate/js/vendor/modernizr-2.6.2.min.js"></script>
        <script type='text/javascript' src='client/js/vendor/jquery/jquery-2.1.1.min.js'></script> 



    <metadata ng-include src="'client/themes/'+appController.theme+'/head.html'" ></metadata>
</head>
<body>
    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div> 
        <div ng-include src="'client/themes/'+appController.theme+'/body.html'" ></div>
    </div>
    <!-- <div ng-view></div>-->
    <!-- In production use:
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/x.x.x/angular.min.js"></script>
    -->
    <!--
    <script src="client/bower_components/angular/angular.js"></script>
    <script src="client/bower_components/angular-route/angular-route.js"></script> 
    <script src="client/js/vendor/angular/angular-ui-router.js"></script>
    <script src="client/js/vendor/angular/angular-resource.min.js"></script>
    <script src="client/js/vendor/angular/angular-mocks.js"></script>
    -->

    <script src="client/js/vendor/angular/angular.min.js" ></script>
    <script src="client/js/vendor/angular/angular-ui-router.js" ></script>
    <script src="client/js/vendor/angular/angular-resource.min.js" ></script>
    <!-- <script src="js/vendor/angular/angular-mocks.js" ></script> -->

    <!-- controllers --> 
    <script src="client/app/app.js"></script>
    <script src="client/app/controller/GroupMatrixController.js"></script>
    <script src="client/app/controller/SettingConroller.js"></script>
    <script src="client/app/controller/StudentOverviewController.js"></script>
    <script src="client/app/controller/SelectGroupController.js"></script>
    
    <script src="client/app/controller/GroupCreateController.js"></script>
    <script src="client/app/controller/GroupOverviewController.js"></script>
    <script src="client/app/controller/StudentAssignController.js"></script>
    <script src="client/app/controller/StudentRetainController.js"></script>

    <!-- services -->
    <script src="client/common/sharedVariable/globalFactory.js"></script>
    <script src="client/common/sharedVariable/config/config.js"></script>
    <script src="client/common/sharedVariable/language/language.js"></script>
    <script src="client/common/services/common.services.js"></script>


    <script src="client/components/version/version.js"></script>
    <script src="client/components/version/version-directive.js"></script>
    <script src="client/components/version/interpolate-filter.js"></script>
    
    <!-- this is a hard fix for the route and ng-include read app controller for more information -->
    <div ng-controller="routeFixForNGInclude as doNotUseThisVar"></div>
</body>
</html>
