/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function () {
    "use strict";
    angular.
            module("mServerRequest",
                    ["ngResource"]).
            factory("mServerRequest",
                    ["$resource",
                        mServerRequest]);

    function mServerRequest($resource) 
    {
        var commonService = {};
        
        commonService.apiDataSingle = $resource(angularFrameworkPathToClientSide+"/api/json/demo_data_single.json?pid=:productId");
        commonService.apiDataSingleSecond = $resource("/api/result-single/:division/?timestamp=:timestamp");
        //commonService.apiDataSingle = $resource(angularFrameworkPathToClientSide+"/api/json/demo_data_single4.json?pid=:division");
        
        //demo json:
        //commonService.apiSelectGroups = $resource(angularFrameworkPathToClientSide+"/api/json/demo_select_group.json?pid=:productId");
        //live data:
        commonService.apiSelectGroups = $resource("/api/groups/ ");
        
        commonService.userData = $resource("/api/current-me/ ");


        //Demo json:
        //commonService.apiStudent= $resource(angularFrameworkPathToClientSide+"/api/json/demo_student.json?pid=:productId");
        //live data:

        commonService.apiStudent= $resource("/api/groups/:group_id/students/ ", {group_id:'@id'});
        
        commonService.apiStudentGroupResults = $resource("/api/students/:student_id/group_results/", {student_id:'@id'});

        //Demo json:
        //commonService.apiGroupMatrix = $resource(angularFrameworkPathToClientSide+"/api/json/demo_data-matrix-multi2.json?pid=:productId");
        //Live data:
        commonService.apiGroupMatrix = $resource("/api/groups/:group_id/results/", {group_id:'@id'});

        return commonService;
    }

}());


