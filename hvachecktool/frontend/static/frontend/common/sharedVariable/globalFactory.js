(function () {
    "use strict";
    angular.
            module("mGlobalFactory",
                    []).
            factory("mGlobalFactory",
                    ["$resource", "mConfig",
                        'mLanguage', "mServerRequest",
                        mGlobalFactory]);

    function mGlobalFactory($resource, mConfig, mLanguage, mServerRequest) {
        //system code (ignore)
        var sharedService = {};
        //var lang = getLanguage(mLanguage, mConfig);

        sharedService.lang = function (sKey, sPlaceHolder, sStartingPoint) {
            return getLanguage(mLanguage.init, sKey, mConfig.defaultLanguage, sPlaceHolder, sStartingPoint);
        };


        sharedService.scrollToTop = function () {
            // go to top on startup
            setTimeout(function () {
                window.scrollTo(0, 0);
            }, 50);
        };


        //start your code under here
        var updateBindingValue = function (sVariable, sNewValue) {
            setTimeout(function () {
                $('.ng-custom-update-' + sVariable).html(sNewValue);
            }, 0);
        };

        sharedService.updateBindingValue = updateBindingValue;


        sharedService.language = "";
        sharedService.contentTitle = "from mGlobalFactory";
        sharedService.contentBreadCrumbs = [{url: "", name: ""}];

        // getters en setters
        sharedService.setContentTitle = function (newValue) {
            updateBindingValue("contentTitle", newValue);
            sharedService.contentTitle = newValue;
        };

        sharedService.getContentTitle = function () {
            return sharedService.contentTitle = newValue;
        };

        sharedService.setContentBreadCrumbs = function (newValue) {
            updateBindingValue();
            sharedService.contentBreadCrumbs = newValue;
        };

        sharedService.getContentBreadCrumbs = function () {
            return sharedService.contentBreadCrumbs;
        };

        /**
         * select group view
         */
        sharedService.groupNameSelected = "";
        sharedService.groupIdSelected = false;
        sharedService.allGroups = [];
  


        //system
        sharedService.arrayToObject = function (arr) {
            var rv = {};
            for (var i = 0; i < arr.length; ++i)
                rv[i] = arr[i];
            return rv;
        }

        sharedService.arrayToUniqueArray = function (arr) {
            var temp = {};
            for (var i = 0; i < arr.length; i++)
                temp[arr[i]] = true;
            var r = [];
            for (var k in temp)
                r.push(k);
            return r;
        }

        return sharedService;
    }


//system code (ignore)
//get right language pack (ignore this code it is a system code)
    var getLanguage = function (mLanguage, sKey, sLanguagePrefix, sPlaceHolder, sStartingPoint) {
        if (typeof sStartingPoint === "string") {
            var objLang = eval("mLanguage[0]" + sStartingPoint);
            try {
                return objLang[sKey][sLanguagePrefix];
            }
            //when nothing has been found put placeholder
            catch (err) {
                console.error("language location not found: ( mLanguage" + sStartingPoint + "['" + sKey + "']['" + sLanguagePrefix + "'] ) replaced by placeholder: " + sPlaceHolder);
                return sPlaceHolder;
            }
        }
        //when nothing has been found put placeholder
        if (typeof mLanguage[0][sKey][sLanguagePrefix] == "undefined") {
            return sPlaceHolder;
        }
        return mLanguage[0][sKey][sLanguagePrefix];
    };


}());