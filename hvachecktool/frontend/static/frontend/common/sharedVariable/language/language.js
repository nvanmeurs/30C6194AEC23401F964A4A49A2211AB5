//http://msdn.microsoft.com/en-us/library/ee825488(v=cs.20).aspx
(function () {
    "use strict";
    angular.
            module("mLanguage",
                    []).
            factory("mLanguage",
                    ["$resource",
                        mLanguage]);

    function mLanguage($resource) {
        var that = {};
        that.init = {};
        
        /**
         * ENGLISH-USA LANGUAGE
         */
        var objLanguage = [{
                //header text
                search: {
                    "en-US": "Search",
                    "nl-NL": "Zoeken"
                },
            
                showAll: {
                    "en-US": "Show all",
                    "nl-NL": "Toon alles"
                },
                toggleInfobar: {
                    "en-US": "Toggle Infobar",
                    "nl-NL": "Toggle Infobar"
                },
                contentTitle: {
                    "en-US": "Select Group",
                    "nl-NL": "Selecteer Groep/Klas"
                },
                groupSelected: {
                    "en-US": "Group Selection",
                    "nl-NL": "Groep Geselecteerd"
                },
                topics: {
                    "t01":{
                        "en-US": "Dagboeken",
                        "nl-NL": "Dagboeken"
                     },
                     "t02":{
                        "en-US": "Grootboekrekeningen",
                        "nl-NL": "Grootboekrekeningen"
                     },
                     "t03":{
                        "en-US": "BTW-codes",
                        "nl-NL": "BTW-codes"
                     },
                     "t04":{
                        "en-US": "Betalingscondities",
                        "nl-NL": "Betalingscondities"
                     },
                     "t05":{
                        "en-US": "Relaties",
                        "nl-NL": "Relaties"
                     },
                     "t06":{
                        "en-US": "Artikelgroepen",
                        "nl-NL": "Artikelgroepen"
                     },
                     "t07":{
                        "en-US": "Artikelen",
                        "nl-NL": "Artikelen"
                     },
                     "t08":{
                        "en-US": "Nieuw Boekjaar",
                        "nl-NL": "Nieuw Boekjaar"
                     },
                     "t09":{
                        "en-US": "Instellingen",
                        "nl-NL": "Instellingen"
                     },
                     "t10":{
                        "en-US": "Inkoopfacturen",
                        "nl-NL": "Inkoopfacturen"
                     },
                     "t11":{
                        "en-US": "Verkoopfacturen",
                        "nl-NL": "Verkoopfacturen"
                     },
                     "t12":{
                        "en-US": "Bankboek",
                        "nl-NL": "Bankboek"
                     },
                     "t13":{
                        "en-US": "Overige boekingen",
                        "nl-NL": "Overige boekingen"
                     },
                     
                    
                    //old apikey's/topics
                    "accounts": {
                        "en-US": "Accounts",
                        "nl-NL": "Relaties"
                    },
                    "glaccounts": {
                        "en-US": "GL Accounts",
                        "nl-NL": "Grootboekrekeningen"
                    },
                    "journals": {
                        "en-US": "Journals",
                        "nl-NL": "Dagboeken"
                    },
                    "vats": {
                        "en-US": "VAT Codes",
                        "nl-NL": "BTW Codes"
                    },
                    "paymentconditions": {
                        "en-US": "Payment Conditions",
                        "nl-NL": "Betalingscondities"
                    },
                    "contacts": {
                        "en-US": "Contacts",
                        "nl-NL": "Contacten"
                    },
                    "itemgroups": {
                        "en-US": "Item Groups",
                        "nl-NL": "Artikelgroepen"
                    },
                    "items": {
                        "en-US": "Items",
                        "nl-NL": "Artikelen"
                    },
                    "financialperiods": {
                        "en-US": "Financial Periods",
                        "nl-NL": "Boekjaar/Periode"
                    },
                    "settings": {
                        "en-US": "Settings",
                        "nl-NL": "Instellingen"
                    },
                    "balances": {
                        "en-US": "Opening Balances",
                        "nl-NL": "Beginbalans"
                    },
                    "receivables": {
                        "en-US": "Receivables",
                        "nl-NL": "Debiteuren"
                    },
                    "payables": {
                        "en-US": "Payables",
                        "nl-NL": "Crediteuren"
                    }
                },
                assignments:{
                    "a01_01": {
                        "en-US": "Dagboek kas aanmaken",
                        "nl-NL": "Dagboek kas aanmaken"
                    },
                    "a01_02": {
                        "en-US": "Dagboek inkoop aanmaken",
                        "nl-NL": "Dagboek inkoop aanmaken"
                    },
                    
                    
                    "a02_01": {
                        "en-US": "Grootboekrekening verwijderen",
                        "nl-NL": "Grootboekrekening verwijderen"
                    },
                    "a02_02": {
                        "en-US": "Grootboekrekening vernummeren",
                        "nl-NL": "Grootboekrekening vernummeren"
                    },
                    "a02_03": {
                        "en-US": "Grootboekrekening kopieren",
                        "nl-NL": "Grootboekrekening kopieren"
                    },
                    "a02_04": {
                        "en-US": "Grootboekrekening aanmaken",
                        "nl-NL": "Grootboekrekening aanmaken"
                    },
                    
                    
                    "a03_01": {
                        "en-US": "Veranderen BTW code",
                        "nl-NL": "Veranderen BTW code"
                    },
                    
                    
                    "a03_02": {
                        "en-US": "BTW-codes aanmaken",
                        "nl-NL": "BTW-codes aanmaken"
                    },
                    
                    
                    "a04_01": {
                        "en-US": "Betalingscondities aanmaken",
                        "nl-NL": "Betalingscondities aanmaken"
                    },
                    
                    
                    "a05_01": {
                        "en-US": "Klant aanmaken",
                        "nl-NL": "Klant aanmaken"
                    },
                    "a05_02": {
                        "en-US": "Leverancier aanmaken",
                        "nl-NL": "Leverancier aanmaken"
                    },
                    "a05_03": {
                        "en-US": "Maak van leverancier een klant",
                        "nl-NL": "Maak van leverancier een klant"
                    },
                    
                    "a06_01": {
                        "en-US": "Artikelgroepen aanmaken",
                        "nl-NL": "Artikelgroepen aanmaken"
                    },
                    
                    "a07_01": {
                        "en-US": "Artikelen aanmaken",
                        "nl-NL": "Artikelen aanmaken"
                    },
                    
                    "a08_01": {
                        "en-US": "Nieuw boekjaar aanmaken",
                        "nl-NL": "Nieuw boekjaar aanmaken"
                    },
                    
                    "a09_01": {
                        "en-US": "Grootboekkoppelingen instellen",
                        "nl-NL": "Grootboekkoppelingen instellen"
                    },
                    "a09_02": {
                        "en-US": "Betalingscondities instellen",
                        "nl-NL": "Betalingscondities instellen"
                    },
                    "a09_03": {
                        "en-US": "Beginbalans",
                        "nl-NL": "Beginbalans"
                    },
                    "a10_01": {
                        "en-US": "Inkoopfacturen aanmaken",
                        "nl-NL": "Inkoopfacturen aanmaken"
                    },
                    "a10_02": {
                        "en-US": "Nog te betalen bedragen",
                        "nl-NL": "Nog te betalen bedragen"
                    },
                    "a10_03": {
                        "en-US": "Vooruitbetaalde bedragen",
                        "nl-NL": "Vooruitbetaalde bedragen"
                    },
                    "a11_01": {
                        "en-US": "Verkoopfacturen aanmaken",
                        "nl-NL": "Verkoopfacturen aanmaken"
                    },
                    "a11_02": {
                        "en-US": "Creditfactuur",
                        "nl-NL": "Creditfactuur"
                    },
                    "a11_03": {
                        "en-US": "Betalingskorting",
                        "nl-NL": "Betalingskorting"
                    },
                    "a12_01": {
                        "en-US": "Betalingen handmatig verwerken",
                        "nl-NL": "Betalingen handmatig verwerken"
                    },
                    "a12_02": {
                        "en-US": "Betalingen automatisch verwerken",
                        "nl-NL": "Betalingen automatisch verwerken"
                    },
                    "a13_01": {
                        "en-US": "Afschrijvingen",
                        "nl-NL": "Afschrijvingen"
                    },
                    "a13_02": {
                        "en-US": "BTW",
                        "nl-NL": "BTW"
                    },
                    "a13_03": {
                        "en-US": "Afsluiten Boekjaar",
                        "nl-NL": "Afsluiten Boekjaar"
                    },
                    
                    
                },
                 fieldNames: {
                    "code": {
                        "en-US": "Code",
                        "nl-NL": "Code"
                    },
                    "type": {
                        "en-US": "Type",
                        "nl-NL": "Type"
                    },
                    "description": {
                        "en-US": "Description",
                        "nl-NL": "Omschrijving"
                    },
                    "glaccountcode": {
                        "en-US": "GL Account",
                        "nl-NL": "Grootboekrekening"
                    },
                    "gltopaycode": {
                        "en-US": "VAT To Pay",
                        "nl-NL": "Af Te Dragen BTW"
                    },
                    "gltoclaimcode": {
                        "en-US": "VAT To Claim",
                        "nl-NL": "Te Ontvangen BTW"
                    },
                    "percentage": {
                        "en-US": "Percentage",
                        "nl-NL": "Percentage"
                    },
                    "vatboxlink": {
                        "en-US": "VAT Box Link",
                        "nl-NL": "BTW Koppeling"
                    },
                    "paymentdays": {
                        "en-US": "Payment Days",
                        "nl-NL": "Vervaldatum"
                    },
                    "name": {
                        "en-US": "Name",
                        "nl-NL": "Naam"
                    },
                    "address": {
                        "en-US": "Address",
                        "nl-NL": "Adres"
                    },
                    "status": {
                        "en-US": "Status",
                        "nl-NL": "Status"
                    },
                    "issupplier": {
                        "en-US": "Supplier",
                        "nl-NL": "Leverancier"
                    },
                    "issales": {
                        "en-US": "Klant",
                        "nl-NL": "Klant"
                    },
                    "vatnumber": {
                        "en-US": "VAT Number",
                        "nl-NL": "BTW Nummer"
                    },
                    "paymentconditionpurchase": {
                        "en-US": "Payment Condition",
                        "nl-NL": "Betalingsconditie"
                    },
                    "glaccountpurchase": {
                        "en-US": "GL Account",
                        "nl-NL": "Grootboekrekening"
                    },
                    "purchasevatcode": {
                        "en-US": "VAT Code",
                        "nl-NL": "BTW Code"
                    },
                    "accountname": {
                        "en-US": "Account",
                        "nl-NL": "Relatie"
                    },
                    "fullname": {
                        "en-US": "Name",
                        "nl-NL": "Naam"
                    },
                    "glrevenuecode": {
                        "en-US": "GL Account",
                        "nl-NL": "Grootboekrekening"
                    },
                    "itemgroupcode": {
                        "en-US": "Item Group",
                        "nl-NL": "Artikelgroep"
                    },
                    "salesprice": {
                        "en-US": "Sales Price",
                        "nl-NL": "Verkoopprijs"
                    },
                    "salesvatcode": {
                        "en-US": "VAT Code",
                        "nl-NL": "BTW Code"
                    },
                    "unitdescription": {
                        "en-US": "Unit",
                        "nl-NL": "Eenheid"
                    },
                    "isfractionalloweditem": {
                        "en-US": "Fraction Allowed",
                        "nl-NL": "Deelbaar Toegestaan"
                    },
                    "startdate": {
                        "en-US": "Start Date",
                        "nl-NL": "Begin Datum"
                    },
                    "finperiod": {
                        "en-US": "Financial Period",
                        "nl-NL": "Financi&euml;le Periode"
                    },
                    "gldiscountsales": {
                        "en-US": "Sales Discount",
                        "nl-NL": "Verkoopkortingen"
                    },
                    "glpaymentdifferencepurchase": {
                        "en-US": "Payment Difference Purchase",
                        "nl-NL": "Betalingsverschillen Inkoop"
                    },
                    "gldiscountpurchase": {
                        "en-US": "Purchase Discount",
                        "nl-NL": "Inkoopkortingen"
                    },
                    "paymentpurchase": {
                        "en-US": "Payment Condition Purchase",
                        "nl-NL": "Betalingsconditie Inkoop"
                    },
                    "paymentsales": {
                        "en-US": "Payment Condition Sales",
                        "nl-NL": "Betalingsconditie Verkoop"
                    },
                    "allowreopenentry": {
                        "en-US": "Allow Reopen Entry",
                        "nl-NL": "Heropenen Toestaan"
                    },
                    "splitoutstandingitemsperinvoice": {
                        "en-US": "Split Automatically",
                        "nl-NL": "Automatisch Splitsen"
                    },
                    "perioddatecheck": {
                        "en-US": "Period Date Check",
                        "nl-NL": "Periode Datum Controle"
                    },
                    "open": {
                        "en-US": "Opening Balance",
                        "nl-NL": "Openingsbalans"
                    },
                    "close": {
                        "en-US": "Closing Balance",
                        "nl-NL": "Eindbalans"
                    },
                    "debit": {
                        "en-US": "Debit Balance",
                        "nl-NL": "Debet Balans"
                    },
                    "credit": {
                        "en-US": "Credit Balance",
                        "nl-NL": "Credit Balans"
                    },
                    "invoicenumber": {
                        "en-US": "Invoice",
                        "nl-NL": "Factuur"
                    },
                    "amount": {
                        "en-US": "Amount",
                        "nl-NL": "Bedrag"
                    },
                    "journalcode": {
                        "en-US": "Journal",
                        "nl-NL": "Dagboek"
                    }
                }
                
            }];
        
        
        that.init= {};
        that.init = objLanguage;

        return that;
    }

}());