var angularFrameworkDefaultTheme = "basic";
var angularFrameworkPathToClientSide = "static/frontend";
(function () {
    "use strict";
    angular.
            module("mConfig",
                    []).
            factory("mConfig",
                    ["$resource",
                        mConfig]);

    function mConfig($resource) {
        var sharedConfig = this;
        sharedConfig.title = "ABICUS EDU";
        sharedConfig.currency = "$";
        sharedConfig.defaultLanguage = "nl-NL";
        sharedConfig.theme = angularFrameworkDefaultTheme;
        sharedConfig.pathToClientSide = angularFrameworkPathToClientSide;
        //sharedConfig.theme = "empty";
        return sharedConfig;
    }

}());

