(function () {
    "use strict";
    var app = angular.
            module("app", ["cStudentOverview", "cSelectGroup", "cGroupMatrix", "cSetting", "cGroupCreate", "cGroupOverview", "cStudentAssign", "cStudentRetain",
                "ui.router", "routeFixForNGInclude",
                "mServerRequest", "mGlobalFactory",
                "mConfig", "mLanguage"]).
            config(function ($stateProvider, $urlRouterProvider) {
                $urlRouterProvider.otherwise('/selectgroup');
                $stateProvider
                        .state({
                            name: 'home/studentoverview',
                            url: '/studentoverview',
                            templateUrl: angularFrameworkPathToClientSide + '/themes/' + angularFrameworkDefaultTheme + '/template/StudentOverview.html',
                        });
                $stateProvider
                        .state({
                            name: 'home/groupmatrix',
                            url: '/groupmatrix',
                            templateUrl: angularFrameworkPathToClientSide + '/themes/' + angularFrameworkDefaultTheme + '/template/GroupMatrix.html',
                        });
                $stateProvider
                        .state({
                            name: 'home/setting',
                            url: '/setting',
                            templateUrl: angularFrameworkPathToClientSide + '/themes/' + angularFrameworkDefaultTheme + '/template/SettingsView.html',
                        });

                $stateProvider
                        .state({
                            name: 'home/selectgroup',
                            url: '/selectgroup',
                            templateUrl: angularFrameworkPathToClientSide + '/themes/' + angularFrameworkDefaultTheme + '/template/SelectGroup.html',
                        });

                $stateProvider
                        .state({
                            name: 'home/groupcreate',
                            url: '/groupcreate',
                            templateUrl: angularFrameworkPathToClientSide + '/themes/' + angularFrameworkDefaultTheme + '/template/GroupCreate.html',
                        });
                $stateProvider
                        .state({
                            name: 'home/groupoverview',
                            url: '/groupoverview',
                            templateUrl: angularFrameworkPathToClientSide + '/themes/' + angularFrameworkDefaultTheme + '/template/GroupOverview.html',
                        });
                $stateProvider
                        .state({
                            name: 'home/studentassign',
                            url: '/studentassign',
                            templateUrl: angularFrameworkPathToClientSide + '/themes/' + angularFrameworkDefaultTheme + '/template/StudentAssign.html',
                        });
                $stateProvider
                        .state({
                            name: 'home/studentretain',
                            url: '/studentretain',
                            templateUrl: angularFrameworkPathToClientSide + '/themes/' + angularFrameworkDefaultTheme + '/template/StudentRetain.html',
                        });



            }).
            controller("app", ["$scope", "$timeout", "mServerRequest", "mGlobalFactory", "mConfig", "mLanguage", cApp]);


    function cApp($scope, $timeout, serverRequest, globalFactory, config, language, $location) {
        var that = this;
        $scope.lang = globalFactory.lang;
        $scope.pageContentHeight = window.innerHeight - 40;
        
        location.href = "#/selectgroup";
        that.theme = config.theme;
        that.pathToClientSide = config.pathToClientSide;
        that.title = config.title;

        that.contentTitle = globalFactory.contentTitle;
        that.groupNameSelected = globalFactory.groupNameSelected;
        that.allGroups = globalFactory.allGroups;
        that.isGroupIdSelectedSelected = function () {
            return (globalFactory.groupIdSelected === null) ? true : false;
            ;
        }

        that.contentBreadCrumbs = globalFactory.contentBreadCrumbs;

        that.objNavigation = [
            {
                name: "Selecteer klas",
                url: "#/selectgroup",
                icon: "fa-users"
            },
            {
                name: "Student overzicht",
                url: "#/studentoverview",
                icon: "fa-user"
            }, {
                name: "Resultaten per klas",
                url: "#/groupmatrix",
                icon: "fa-tasks"
            },/* {
                name: "Instellingen",
                url: "#/setting",
                icon: "fa-cog"
            },

             {
             name: "groupcreate",
             url: "#/groupcreate",
             icon: "fa-cog"
             },
             {
             name: "groupoverview",
             url: "#/groupoverview",
             icon: "fa-cog"
             },
             {
             name: "studentassign",
             url: "#/studentassign",
             icon: "fa-cog"
             },
             {
             name: "studentretain",
             url: "#/studentretain",
             icon: "fa-cog"
             }
             */


        ];

        that.isNavigationUrlSelectable = function (url) {
            
            if (url === "#/selectgroup" || url === "#/setting") {
                return false;
            }
            return (globalFactory.groupNameSelected == false && globalFactory.groupIdSelected == false);
        };

        that.isLocationActive = function (route) {
            return route === window.location.hash;
        };

        that.hasAssignmentGoodGraded = function (objToCheck) {
            var countFalseResult = dojox.json.query("$.[?(@.result =='0')]", objToCheck).length;
            return  {
                countResultFalse: countFalseResult,
                boolean: !(countFalseResult > 0)
            };
        };

        that.numOfGoodGradedInPercentage = function (objToCheck) {
            var iPositive = dojox.json.query("$.[?(@.result =='0')]", objToCheck).length;
            var iFalse = dojox.json.query("$.[?(@.result =='1')]", objToCheck).length;
            return  100 - parseInt(100 / ((iPositive + iFalse) / iPositive));
        };

        //getters and setters
        that.getTheme = function () {
            return that.theme;
        };

        that.getPathToClientSide = function () {
            return that.pathToClientSide;
        };


     

    }

}());

//system code
/**
 * When the application starts the routing is first done and then the ng-include will be executed this will prevent this. 
 * this will re-route when the ng-include is done executing 
 * 
 * this function is only executed only once at the start of the application)
 */
(function () {
    "use strict";
    var app = angular.
            module("routeFixForNGInclude", []).
            controller("routeFixForNGInclude", function ($scope, $state) {
                // go to top on startup
                window.scrollTo(0, 0);
            });

}());
