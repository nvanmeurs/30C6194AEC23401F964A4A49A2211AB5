(function () {
    "use strict";
    var app = angular.
            module("cStudentOverview", []).
            controller("cStudentOverview", ["mGlobalFactory", "mServerRequest", StudentOverviewController]);

    function StudentOverviewController(globalFactory, mServerRequest) {
        globalFactory.scrollToTop();
        
        var that = this;
        that.lang = {};

        globalFactory.setContentTitle("Student overzicht");
        that.lang.next = "NEXT";
        that.lang.previous = "PREVIOUS";
        that.lang.detailHeaderStudentOverview = "Details";
        that.lang.tableHeaderStudentOverview = "Overzicht";
        that.lang.searchHeaderStudentOverview = "Zoek";

        that.lang.noDataFound = {};
        that.lang.noDataFound.explain = "Geen student data gevonden";
        that.lang.noDataFound.link = "Klik here om een nieuw group te kiezen";

        that.lang.dataLoading = {};
        that.lang.dataLoading.explain = "Laden ...";

        that.lang.noDataFoundForSelectedDivision = {};
        that.lang.noDataFoundForSelectedDivision.explain = "Geen data gevonden voor de geselecteerde student";

        that.lang.explainSelectStudent = "Selecteer een student";

        that.currentPage = 0;
        that.pageSize = 10;

        that.apiStudent = [];
        
        globalFactory.refreshStudentList = that.refreshStudentList = function (){
            that.apiStudent = [];
            that.loading = true;
             mServerRequest.apiStudent.query({group_id: globalFactory.groupIdSelected}, {}, function (data) {
                that.apiStudent = data;
                that.loading = false;
            });
        };
        
        that.refreshStudentList();
        
        that.refreshStudentScores = function (){
            that.setStudentSelected(that.selectedStudentId);
        };
        
        that.setSelectedStudent = function(student_id){
            that.selectedStudentid = student_id;
            var src = $("#studentResult").attr("src");
            var slices = src.split("/");
            var newSrc = "";
            for(var i = 0; i < (slices.length -1); i++)
                newSrc = (i) ? newSrc + "/" + slices[i] : slices[i];
            
            newSrc = newSrc + "/StudentResult.html?student_id=" + student_id;
            $("#studentResult").attr("src", newSrc);
        };

        // For todays date;
        var getTimeToday = function () {
            var timeNow = new Date();
            var hours = timeNow.getHours();
            var minutes = timeNow.getMinutes();
            var seconds = timeNow.getSeconds();
            var timeString = "" + ((hours > 12) ? hours - 12 : hours);
            timeString += ((minutes < 10) ? ":0" : ":") + minutes;
            timeString += ((seconds < 10) ? ":0" : ":") + seconds;
            timeString += (hours >= 12) ? " P.M." : " A.M.";
            return timeString;
        }
        
    }



}());


