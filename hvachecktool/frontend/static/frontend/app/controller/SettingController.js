(function(){
    "use strict";
    var app = angular.
            module("cSetting",[]).
            controller("cSetting",["mGlobalFactory", "mServerRequest", "$http", SettingController]);
    
    function SettingController(globalFactory, mServerRequest, $http)
    {
        globalFactory.scrollToTop();
        
        var that  = this;
        globalFactory.setContentTitle("Instellingen");
        
        that.refreshStudentList = function (){
            that.apiStudentWithoutGroup = [];
             mServerRequest.apiStudent.query({group_id: "null"}, {}, function (data) {
                $.each(data, function(i){data[i].selected = false;});
                that.apiStudentWithoutGroup = data;
            });
        };
        
        that.refreshGroupList = function (){
            that.apiGroups = [];
             mServerRequest.apiSelectGroups.query(function (data) {
                $.each(data, function(i){data[i].selected = false;});
                that.apiGroups = data;
            });
        };
        
        that.assignToGroup = function(studentIDs, groupID)
        {
            //$.cookie("csrftoken")
            var payload = JSON.stringify(studentIDs);
            
            //post to endpoint end get resultmessage
            $http.defaults.headers.common['X-CSRFToken']= $.cookie("csrftoken");
            mServerRequest.apiAssignStudent.save(
                {group_id:groupID}, payload, 
                function(data)
                {
                    console.log(data);
                }
            );
        };
        
        that.resetGroups = function(){
            $.each(that.apiGroups, function(i){that.apiGroups[i].selected = false;});
        };
        
        that.resetStudents = function(){
            $.each(that.apiStudentWithoutGroup, function(i){that.apiStudentWithoutGroup[i].selected = false;});
        };
    
        
        that.toggleSelected = function(element, toggleAll)
        {
            //check if all true
            var allTrue = true;
            $.each(that.apiStudentWithoutGroup, function(i){
                if(!that.apiStudentWithoutGroup[i].selected)
                    allTrue = false;
            });
            
            $.each(that.apiStudentWithoutGroup, function(i){that.apiStudentWithoutGroup[i].selected = !allTrue;});
        };
        
        that.assign = function()
        {
            var studentIDs = [];
            $.each(that.apiStudentWithoutGroup, function(i){
                if(that.apiStudentWithoutGroup[i].selected)
                    studentIDs.push(that.apiStudentWithoutGroup[i].id);
            });

            if(studentIDs.length)
            {
                var checkedInput = $("#groupList .group input[type='radio']:checked");
                var selectedGroup = (checkedInput.length) ? checkedInput.data("id") : null;
                if(selectedGroup)
                    that.assignToGroup(studentIDs, selectedGroup);
                else 
                    alert("No group was selected");
                    //@todo: warning message for not having selected a group
            } 
            else 
                alert("no students selected");
                //@todo: warning message for not having selected any students to assign
        };
        
        that.refreshStudentList();
        that.refreshGroupList();
        
        that.currentPage = 0;
        that.pageSize = 30;
        
    }
    
}());


