angular.module("viewOnly", ["mGlobalFactory", "ngResource", "mConfig", "mLanguage", "mServerRequest"]).
controller('mainController', ["mGlobalFactory", "$http", function(mGlobalFactory, $http)
{
    this.g=mGlobalFactory;
    this.results = [];
    this.loading = true;
    this.validStudent = false;
    this.message = "";
    
    var that = this;
    
    this.getParameterByName = function(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    
    this.refreshStudentScores = function()
    {
        that.loading = true;
        this.validStudent = false;
        
        //@todo: change this to the line above when the endpoint is ready
        $http.get("/api/students/" + student_id + "/results/ ").
        success(function(data, status) {
            that.loading = false;
            if(status === 200){
                that.validStudent = true;
                that.results = data;
            }  
            else
                that.message = "{Something went wrong while trying to fetch the results}";
        }).
        error(function() {
            that.message = "{Something went wrong while trying to fetch the results}";
        });
    };
    
    this.setName = function(){
        $http.get("/api/students/" + student_id + "/ ").
        success(function(data){
            that.studentName = data.name;
        });
    }

    var student_id = this.getParameterByName("student_id");
    if(student_id.length){
        this.refreshStudentScores();
        this.setName();
    }
    else{
        this.loading = false;
        this.validStudent = false;
    }
        
    
    this.numOfGoodGradedInPercentage = function () {
        /* Amount of false positives that should be ignored. */
        var iOffset = 7;

        var iWrong = dojox.json.query("$.[?(@.result =='0')]", that.results).length;
        var iRight = dojox.json.query("$.[?(@.result =='1')]", that.results).length - iOffset;
        var iTotal = (iWrong + iRight) - iOffset;

        var iPercentage = iRight/iTotal * 100;

        return parseInt(iPercentage);
    };
    
    this.hasAssignmentGoodGraded = function (objToCheck) {
        var countFalseResult = dojox.json.query("$.[?(@.result =='0')]", objToCheck).length;
        return  {
            countResultFalse: countFalseResult,
            boolean: !(countFalseResult > 0)
        };
    };
    
    that.setDialogBox = function (title, msg, result) {
        setDialogBoxResultTrue(title, msg);
    };

    var setDialogBoxResultTrue = function (title, msg) {
        bootbox.dialog({
            message: msg,
            title: title,
            buttons: {
                success: {
                    label: "OK",
                    className: "btn-primary",
                    callback: function () {

                    }
                }
            }
        });
    };
    
    /*
    * 
    * BELOW THIS POINT WILL BE CODE FOR ANIMATING THE STUDENT RESULTS. 
    * (HACK BECAUSE IT IS NOW BINDING WITH THE DEFAULT ACC-MENU)
    */

    
    this.onClickSubItem = function(element){
        element = $(element.currentTarget);
        
        var LIs = element.closest('ul.student-submenu').children('li');
        element.closest('li').addClass('clicked');
        $.each(LIs, function (i) {
            if ($(LIs[i]).hasClass('clicked')) {
                $(LIs[i]).removeClass('clicked');
                return true;
            }
            
            $(LIs[i]).find('ul.student-submenu:visible').slideToggle();
            $(LIs[i]).removeClass('open');
        });
        
        
        if (element.siblings('ul.student-submenu:visible').length > 0)
            element.closest('li').removeClass('open');
        else
            element.closest('li').addClass('open');
        
        
        element.siblings('ul.student-submenu').slideToggle({
            duration: 200,
            progress: function () {
                if ($(this).closest('li').is(":last-child")) { //only scroll down if last-child
                    $("#student-results").animate({scrollTop: $("#student-results").height()}, 0);
                }

            },
            complete: function () {
                $("#student-results").getNiceScroll().resize();
            }
        });
    };
    
    this.leftbarScrollShow = function () {
        
        $("#sidebar").getNiceScroll().hide();
        $("#sidebar").getNiceScroll().resize();
    };

    var liHasUlChild = $('li').filter(function () {
        return $(this).find('ul.student-submenu').length;
    });
    $(liHasUlChild).addClass('hasChild');

}]);    
