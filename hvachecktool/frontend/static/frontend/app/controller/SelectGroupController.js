(function(){
    "use strict";
    var app = angular.
            module("cSelectGroup",[]).
            controller("cSelectGroup",["$rootScope", "$location", "mGlobalFactory","mServerRequest", selectGroupController]);
    
    function selectGroupController($rootScope, $location, globalFactory,mServerRequest, $route){
        var that  = this;
        that.lang = {};
        
        globalFactory.setContentTitle("Selecteer Klas");
        
        that.lang.next = "NEXT";
        that.lang.previous = "PREVIOUS";
        that.lang.tableHeaderSelectGroup = "Overzicht";
        that.lang.searchHeaderSelectGroup = "Zoek";
        
        that.currentPage = 0;
        that.pageSize = 10;
        
        that.groupIdSelected = globalFactory.groupIdSelected;
        $rootScope.groupNameSelected = globalFactory.groupNameSelected;
        $rootScope.allGroups;
        
        
        that.setGroupIdPlusGroupName = function(iNewValue, sNewValue){
            // go to top on startup
            window.scrollTo(0, 0);  
            globalFactory.groupIdSelected = iNewValue;
            globalFactory.groupNameSelected = sNewValue;

            that.groupIdSelected = iNewValue;
            $rootScope.groupNameSelected = sNewValue;

            var studentView = "/studentoverview";
            
            if($location.path() == studentView)
                globalFactory.refreshStudentList();
            else 
                location.href = "#" + studentView;
            

        };
        

        
        that.loading = true;
        //get all groups ajax request
        mServerRequest.apiSelectGroups.query(function (data) {
            $rootScope.allGroups = data;
            that.apiSelectGroups = data;
            globalFactory.allGroups = data;
            that.loading = false;
        });
    }
    
}());
