(function () {
    "use strict";
    var app = angular.
            module("cGroupMatrix", []).
            controller("cGroupMatrix", ["$scope", "$timeout", "mGlobalFactory", "mServerRequest", GroupMatrixController]);

    function GroupMatrixController($scope, $timeout, globalFactory, mServerRequest) {
        globalFactory.scrollToTop();
        this.g = globalFactory;
        var that = this;
        $scope.studentCSSTHWidth = $(".group-matrix .student-th").css('width');
        $scope.isTableLoading = true;
        
        that.apiGroupMatrix = [];
        that.apiGroupMatrix.data = [{}];
        that.allStudents = [];
        that.allTopics = [];
        that.allAssignments;
        that.allAssignmentResultSortByStudent;
        that.allAssignmentsSortByTopic;

        globalFactory.setContentTitle("Resultaten per klas");

        
        //fetch all studentID's for which data should be fetched
        var initStudents = function()
        {
             mServerRequest.apiStudent.query({group_id: globalFactory.groupIdSelected}, {}, function (data) {      
                $.each(data, function(i){
                    var final = (i === data.length -1) ? true : false;
                    apiGroupMatrix(data[i], final);
                });
            });
        };
        
        //get all groups ajax request
        var apiGroupMatrix = function (data, final){
            if (globalFactory.groupIdSelected !== false) {
                
                mServerRequest.apiStudentGroupResults.query({student_id: data.id}, {}, function(data)
                {   
                    for (var first in data[0]) break;
                    that.apiGroupMatrix.data[0][first] = data[0][first];
                    
                    if(final){
                        $scope.isTableLoading = false; //.sort(function(a, b){return a-b})
                        var tempTopics = globalFactory.arrayToUniqueArray(generateTopic(that.apiGroupMatrix.data));
                        tempTopics.sort();
                        that.allTopics = globalFactory.arrayToObject(tempTopics);
                        
                        that.allStudents = generateStudent(that.apiGroupMatrix.data);
                        that.allAssignmentsSortByTopic = generateArrayAssignmentSortByGroup(that.apiGroupMatrix.data);
                    }
                });
                
            } else {
                that.apiGroupMatrix.data = [];
            }
        };
        
        that.refreshGroupMatrix = function (){
            $scope.isTableLoading = true;
            initStudents();
        };
        
        initStudents();
        //apiGroupMatrix();
        
        that.countColSpanTopic = function (topicName, obj) {
            var topic = dojox.json.query("$..assignment", dojox.json.query("$.[?(@.topic =='" + topicName + "')]", obj));
            topic = globalFactory.arrayToUniqueArray(topic);
            return topic.length;
        };



        this.getAssignmentResultByStudent = function (assignment, obj) {
            var objReturn = dojox.json.query("$.[?(@.assignment =='" + assignment + "')]", obj);
            var errorcount = dojox.json.query("$..errorcount", objReturn)[0];
            return errorcount;
        };
        
        this.getPositiveResultByAssignment = function (assignment, obj) {
            //var objReturn = dojox.json.query("$.[?(@.assignment =='" + assignment + "')]", obj);
            var objReturn = dojox.json.query("$.[?(@.assignment =='" + assignment + "')]", obj);
            var errorCount = dojox.json.query("$..successcount", objReturn)[0];
           
            return errorCount;
        };

        var generateTopic = function (obj) {
            var topics = dojox.json.query("$..topic", obj);
            //console.log(topics);
            return topics;
        };

        var generateStudent = function (obj) {
            obj = obj[0];
            var objStudent = {};
            for (var key in obj) {
                objStudent[key] = obj[key]['name'];
            }
            return objStudent;
        };

        var generateAssignment = function (obj) {
            var assignment = dojox.json.query("$..assignment", obj);
            return assignment;
        };

        var generateObjectAssignmentSortByGroup = function (obj) {
            var objReturn = {};
            var topics = dojox.json.query("$..topic", obj);
            topics = globalFactory.arrayToUniqueArray(topics);
            for (var i = 0; i < topics.length; i++) {
                objReturn[topics[i]] = globalFactory.arrayToUniqueArray(dojox.json.query("$..assignment", dojox.json.query("$.[?(@.topic =='" + topics[i] + "')]", obj)));
            }
            return objReturn
        };

        var generateArrayAssignmentSortByGroup = function (obj) {
            var arrayReturn = [];
            var topics = dojox.json.query("$..topic", obj);
            topics = globalFactory.arrayToUniqueArray(topics);
            for (var i = 0; i < topics.length; i++) {
                arrayReturn.push.apply(arrayReturn, globalFactory.arrayToUniqueArray(dojox.json.query("$..assignment", dojox.json.query("$.[?(@.topic =='" + topics[i] + "')]", obj))));
            }
            return arrayReturn
        };

        var tableStudentWidth = function () {
            $scope.studentCSSTHWidth = $(".group-matrix .student-th").css('width');
            $scope.timerTableStudentWidth = $timeout(tableStudentWidth, 1500);
        };

        
        $scope.timerTableStudentWidth = $timeout(tableStudentWidth, 100);
        $scope.$on(
                "$destroy",
                function (event) {
                    $timeout.cancel($scope.timerTableStudentWidth);
                }
        );
        
    }

}());
