(function(){
    "use strict";
    var app = angular.
            module("cGroupOverview",[]).
            controller("cGroupOverview",["mGlobalFactory","mServerRequest",GroupOverviewController]);
    
    function GroupOverviewController(globalFactory, mServerRequest){
        globalFactory.scrollToTop();
        
        var that  = this;
        
        globalFactory.setContentTitle("Group Overview");
        
    }
    
}());
