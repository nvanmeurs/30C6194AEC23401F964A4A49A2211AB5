/**
 * This is a default template how angular must start
 */
(function(){
    "use strict";
    var app = angular.
            module("parentViewAndClassName",[]).
            controller("default",cDefault);
    
    function cDefault(){
        var cDefaultViewModel  = this;
        cDefaultViewModel.defaultValue = "default";
    }
    
}());
