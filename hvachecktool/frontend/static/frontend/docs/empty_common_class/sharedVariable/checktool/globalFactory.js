(function () {
    "use strict";
    angular.
            module("globalFactory",
                    ["globalFactory"]).
            factory("globalFactory",
                    ["$resource",
                        globalFactory]);

    function globalFactory($resource) {
        var sharedService = {};
        sharedService.test = 'yessss';
        return sharedService;
    }

}());