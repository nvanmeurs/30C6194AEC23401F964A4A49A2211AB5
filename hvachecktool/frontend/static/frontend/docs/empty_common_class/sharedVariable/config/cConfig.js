(function () {
    "use strict";
    angular.
            module("config",
                    ["config"]).
            factory("config",
                    ["$resource",
                        mConfig]);

    function mConfig($resource) {
        var sharedConfig  = this;
        sharedConfig.appName = "HVA Check Tool";
        sharedConfig.currency = "$";
        sharedConfig.defaultLanguage = "en-US";
        return sharedConfig;
    }

}());