/**
 * This is a default template how angular must start
 */
(function(){
    "use strict";
    angular.
            module("Default",
                    ["ngResource"]).
            controller("default",cDefault);
    
    function cDefault(){
        var cDefaultViewModel  = this;
        cDefaultViewModel.defaultValue = "default";
    }
    
}());
